import React from 'react'
import Sidebar from './components/sidebar'
import Main from './components/main'
import Topbar from './components/topbar/Topbar'
import Rightmenu from './components/rightmenu/Rightmenu'
import ModalAdd from './components/modal-add/ModalAdd'
import ModalAuth from './components/modal-auth/ModalAuth'

class App extends React.Component {
	componentDidMount() {
		document.body.style.backgroundColor = "transparent";
        document.documentElement.style.backgroundColor = "#f2f2f2";
	}

	render = () => {
  		return (
			<div className="App">
				<Sidebar />
				<div className="container px-0">
					<div className="row mx-0">
						<div className="col-md-12 px-0">
							<Topbar />
						</div>
					</div>
					<div className="row mt-3 mx-0">
						<div className="col-md-8 px-0">
							<Main />
						</div>

						<div className="col-md-4 mx-0 px-0">
							<Rightmenu />
						</div>
					</div>
				</div>
				<ModalAdd />
				<ModalAuth />
			</div>
	  	)
	}
}

export default App;

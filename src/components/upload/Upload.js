import React from 'react'
import { connect } from 'react-redux';
import { bufferAttachFile } from '../../actions/filebuffer'

class Upload extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.input = null
    }

    handleClick() {
        this.input.click();
    }

    handleChange(e) {
        this.props.dispatch(bufferAttachFile(e.target.files[0]))
        this.props.handleFilePick()
    }

    render = () => (
        <div>
            <div onClick={this.handleClick}>
                {this.props.children}
            </div>
            <input 
                ref={input => this.input = input}
                type="file" 
                style={{display: "none"}}
                onChange={e => this.handleChange(e)}
            />
        </div>
    )
}

export default connect()(Upload)
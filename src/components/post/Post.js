import React from 'react'
import { connect } from 'react-redux'
import './index.scss'
// import { postGetHashtags } from '../../actions/PostActions';
import { postLike } from '../../actions/Post'
import Hashtag from '../hashtag/Hashtag';

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.handleClickLike = this.handleClickLike.bind(this);
    }

    componentDidMount() {
        // const { dispatch } = this.props;
        // dispatch(postGetHashtags(this.props.postId));
    }

    handleClickLike() {
        this.props.dispatch(postLike(this.props.id))
    }

    render = () => (
        <div className="post">
            <div className="post-header">
                <a href={"/u/" + this.props.userId} className="post-header-icon" style={{backgroundImage: "url("+this.props.userProfileImageUrl+")"}}> </a>
                <div className="post-header-text">
                    <a href={"/u/" + this.props.userId} className="text-dark post-header-text-title">{this.props.userFullname}</a>
                    {/* <div className="post-header-text-info">{this.props.post_datetime}</div> */}
                    <div className="post-header-text-info">
                        {new Date(this.props.created).toDateString()}
                        {Array.isArray(this.props.hashtags) ? this.props.hashtags.map((hashtag, k) => (
                            <Hashtag key={k}>{hashtag}</Hashtag>
                        )) : ""}
                    </div>
                </div>
            </div>
            <div className={"px-2 pb-3" + (this.props.description ? "" : " d-none")}>{this.props.description}</div>
            <div className="post-body">
                <img className="img-fluid post-body-img" src={this.props.fileUrl} alt="" />
            </div>
            <div className="post-footer">
                <div 
                className={"post-footer-btn btn-like" + (this.props.liked ? " btn-like-active" : "")} 
                onClick={this.handleClickLike
                    // if (this.props.liked) {
                    //     this.props.handleUnlike(this.props.post_ref)
                    // } else {
                    //     this.props.handleLike(this.props.post_ref)
                    // }
                }>
                    <div className="post-footer-btn-icon">
                        <i className="far fa-thumbs-up"></i>
                    </div>
                    <div className="post-footer-btn-text">Like</div>
                    <div className="post-footer-btn-text ml-2">{this.props.likes}</div>
                </div>
                <div 
                className={"post-footer-btn btn-dislike" + (this.props.disliked ? " btn-dislike-active" : "")}
                onClick={id => {
                    if (this.props.disliked) {
                        this.props.handleUndislike(this.props.post_ref)
                    } else {
                        this.props.handleDislike(this.props.post_ref)
                    }
                }}>
                    <div className="post-footer-btn-icon">
                        <i className="far fa-thumbs-down"></i>
                    </div>
                    <div className="post-footer-btn-text">Dislike</div>
                    <div className="post-footer-btn-text ml-2">{this.props.dislikes}</div>
                </div>
                <div className="post-footer-btn">
                    <div className="post-footer-btn-icon">
                        <i className="far fa-comment-dots"></i>
                    </div>
                    <div className="post-footer-btn-text">Comments</div>
                </div>
            </div>
        </div>
    )
}

export default connect()(Post);
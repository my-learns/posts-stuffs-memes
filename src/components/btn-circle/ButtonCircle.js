import React from 'react'
import './ButtonCircle.scss'
import { Link } from 'react-router-dom';

export class ButtonCircle extends React.Component {
    render() {
        if (this.props.link) {
            return (
                <div className="btn-circle-wrapper">
                    <Link 
                        className="btn-circle utooltip"
                        to={this.props.link}
                        >
                        <span>{this.props.children}</span>
                        {!this.props.tooltip ? "" :
                            <div className="utooltip-text">{this.props.tooltip}</div>
                        }
                    </Link>
                </div>
            )
        }

        return (
            <div className="btn-circle-wrapper">
                <div 
                    className="btn-circle utooltip"
                    onClick={this.props.onClick ? this.props.onClick : () => {}}
                    >
                    <span>{this.props.children}</span>
                    {!this.props.tooltip ? "" :
                        <div className="utooltip-text">{this.props.tooltip}</div>
                    }
                </div>
            </div>
        )
    }
};

export class ButtonUpload  extends React.Component {
    constructor(props) {
        super(props);
        this.handleImageUpload = this.handleImageUpload.bind(this);
    }

    handleImageUpload(event) {
        this.props.handleImageUpload(event.target.files[0]);
    }

    render = () => (
        <div className="upload-btn-wrapper">
            <div className="btn-circle">
                <i className="far fa-file-image"></i>
            </div>
            <input 
                type="file" 
                className="btn-circle" 
                style={{width: "100%", height: "100%"}}
                onChange={event => this.handleImageUpload(event)}
            />
        </div>
    )
};
import React from 'react'
import './Topbar.scss'
import ButtonCircle from '../button-circle'
import { connect } from 'react-redux'
import { modalShow } from '../../actions/modal'
// import PostStuff from '../post-stuff/PostStuff'
import PostStuffAdd from '../post-stuff-add'

class Topbar extends React.Component {
    constructor(props) {
        super(props)
        this.handleClickAdd = this.handleClickAdd.bind(this)
    }

    handleClickAdd() {
        this.props.dispatch(modalShow(<PostStuffAdd />))
    }

    render() {
        return (
            <div className="topbar-wrapper">
                <div className="topbar d-flex flex-row-reverse">
                    <ButtonCircle icon="fa fa-plus" color="light" onClick={this.handleClickAdd}/>
                    <ButtonCircle link={`/u/${this.props.user.id}`} image={this.props.user.profileImageUrl} />
                    <div className="flex-fill"></div>
                    
                    <div className="search-wrapper">
                        <input className="search-input" placeholder="Search for stuffs" />
                        <div className="search-button">
                            <ButtonCircle icon="fa fa-search" />
                        </div>
                    </div>

                    <div className="flex-fill"></div>
                    <ButtonCircle icon="fa fa-icons" link="/stuffs" color="light" />
                    <ButtonCircle icon="fa fa-home" link="/" color="light" />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user
})

export default connect(mapStateToProps)(Topbar)
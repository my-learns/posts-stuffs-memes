import React from 'react'
import './PostStuffAdd.scss'
import InputField from '../post-stuff-add-field'
import { connect } from 'react-redux'
import ButtonCircle from '../button-circle/ButtonCircle'
import { modalClear, modalSet } from '../../actions/modal'
import Axios from 'axios'

class PostStuffAdd extends React.Component {
    constructor(props) {
        super(props)
        const { dispatch } = this.props
        dispatch(modalSet({
            mainInputValue: "https://google.com",
            faviconUrl: false
        }));

        this.handleClickNext = this.handleClickNext.bind(this)
    }

    handleClickNext() {
        const { dispatch } = this.props

        Axios
            .post('/req', {url: this.props.mainInputValue})
            .then((response) => {
                if (response.data.ok) {
                    dispatch(modalSet({
                        faviconUrl: response.data.fileUrl
                    }))
                } else {
                    console.log("err");
                }
            })
    }

    render() {
        const { dispatch } = this.props

        return (
            <div className="post-stuff-add-wrapper">
                <div className="post-stuff-add-field-line d-flex">
                    <InputField 
                        icon={this.props.faviconUrl} 
                        onChange={e => dispatch(modalSet({mainInputValue: e.target.value}))}>
                        {this.props.mainInputValue}
                    </InputField>
                    <ButtonCircle  
                        color="blue" 
                        icon="fa fa-arrow-right" 
                        onClick={this.handleClickNext}
                        />
                </div>
        

                <div className="border-bottom mt-3 mb-3"></div>
                <div className="d-flex">
                    <div className="btn b-circle-close" onClick={() => this.props.dispatch(modalClear())}>Close</div>
                    <div className="flex-fill"></div>
                    <div className="btn b-circle-primary">Save</div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => state.modal

export default connect(mapStateToProps)(PostStuffAdd)
import React from 'react'
import './Sidebar.scss'
import { postaddShow } from '../../actions'
import { connect } from 'react-redux'
// import { Link } from 'react-router-dom'
import ButtonCircle from '../button-circle'
import { meRequestInfo, authSignout } from '../../actions/User'

class Sidebar extends React.Component {
    constructor(props) {
        super(props)
        this.handleSignout = this.handleSignout.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(meRequestInfo())
    }

    handleSignout() {
        this.props.dispatch(authSignout())
    }

    render() {
        let profile;
        let addPost;
        let settings;
        let signin;
        let signout;

        if (this.props.isAuthenticated) {
            profile = <ButtonCircle tooltip="Profile" link={`/u/${this.props.id}`} image={this.props.profileImageUrl} />
            addPost = <ButtonCircle tooltip="Add Meme" color="light" icon="far fa-plus" onClick={this.props.handleAddPostClick} />
            settings = <ButtonCircle tooltip="Settings" color="light" icon="fa fa-cog" link="/settings" />
            signout = <ButtonCircle tooltip="Sign out" color="close" icon="far fa-times" onClick={this.handleSignout} />
        } else {
            signin = <ButtonCircle tooltip="Log in" color="light" icon="far fa-sign-in-alt" link="/signin" />
        }

        return (
            <div className="sidebar-wrapper">
                <div className="sidebar">
                    {profile}
                    {addPost}
                    {settings}
                    {signin}
                    <div className="flex-fill"></div>
                    {signout}
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => state.user
const mapDispatchToProps = dispatch => ({
    handleAddPostClick: () => dispatch(postaddShow()),
    dispatch: dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
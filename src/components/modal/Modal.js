import React from 'react'
import "./index.scss"

export default class Modal extends React.Component {
    render = () => {
        if (this.props.active) {
            document.body.style.overflow = "hidden";
        } else {
            document.body.style.overflow = "unset";
        }
    
        return (
            <div 
                className={"cover-wrapper " + (this.props.active ? "" : " d-none")} 
                onClick={this.props.handleClick}>
                <div 
                    className={"cover" + (this.props.active ? "" : " d-none")} 
                    onClick={this.props.handleClick}>
                    <div className="cover-modal" onClick={e => {e.stopPropagation()}}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}
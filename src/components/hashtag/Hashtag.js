import React from 'react'
import './Hashtag.scss'

export default class Hashtag extends React.Component {
    render = () => (
        <div className="hashtag-wrapper">
            <div className="hashtag">
                <div className="hashtag-text">
                    #{this.props.children}
                </div>
                <div 
                    className={"hashtag-close" + (this.props.handleClose ? "" : " d-none")}
                    onClick={this.props.handleClose}
                    >
                    <i className="far fa-times"></i>
                </div>
            </div>
        </div>
    )
}
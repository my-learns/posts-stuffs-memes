import React from 'react'
import './Field.scss'
import ButtonCircle from '../button-circle/ButtonCircle'
import { connect } from 'react-redux';
import { 
    settsField,
    settsFieldSetValue,
    settsFieldUpdate
} from '../../actions/Settings';

class Field extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(settsField(this.props.name))
    }

    handleChange = e => {
        this.props.dispatch(settsFieldSetValue(this.props.name, e.target.value))
    }

    handleClick = value => {
        this.props.dispatch(settsFieldUpdate(this.props.name, value))
    }

    render() {
        let field = this.props.fields.find(field => field.name === this.props.name)

        if (field === undefined) {
            field = {
                value: ''
            }
        }

        let inputStyle = ''
        let btnStyle = ''
        let status = ''

        if (field.changed) {
            btnStyle = 'field-opt-appear';
            inputStyle = 'field-input-toggle';
            status = 'unsaved'
        }

        if (field.updateSuccess && !field.updatePending && !field.changed) {
            btnStyle = 'field-opt-disappear'
            inputStyle = 'field-input-extend'
            status = 'saved'
        }

        if (field.updateFailed) {
            status = 'failed'
        }

        if (status) {
            status = <span className="field-status">&bull;<span className={`field-status field-status-${status}`}>{status}</span></span>
        }

        return (
            <div className="field-group-wrapper">
                <div className="field-label">
                    <span>{this.props.label}</span>
                    {status}
                </div>
                <div className="field-wrapper">
                    <div className="field">
                        <input type="text" className={`field-input ${inputStyle}`} value={field.value} onChange={e => this.handleChange(e)}/>
                        <div className={`field-opt ${btnStyle}`}>
                            <ButtonCircle size="inherit" color="primary" icon="far fa-save" onClick={value => this.handleClick(field.value)}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => state.settings

export default connect(mapStateToProps)(Field);
import React from 'react'
import './TextField.scss'

class InputField extends React.Component {
    render() {
        let icon;

        if (this.props.icon) {
            icon = <div class="post-stuff-add-field-icon" style={{backgroundImage: `url(${this.props.icon})`}}></div>
            
            if (this.props.icon.match(/fa[sbr]?\s*fa.*/)) {
                icon = <i className={this.props.icon}></i>
            }
        }

        return (
            <div className="post-stuff-add-field-wrapper">
                <div className="post-stuff-add-field-icon-wrapper">
                    {icon}
                </div>
                <input 
                    className="post-stuff-add-field-input"
                    onChange={this.props.onChange}
                    value={this.props.children || ""}
                    type="text"
                    placeholder="Type something"
                    />
            </div>
        )
    }
}

export default InputField
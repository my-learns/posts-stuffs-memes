import React from 'react'
import { connect } from 'react-redux'
import Modal from '../modal'
import './ModalUploadImage.scss'
import ButtonCircle from '../button-circle'

class ModalUploadImage extends React.Component {
    render = () => (
        <Modal active={this.props.active} handleClick={this.props.handleHide}>
            <div className="mui-wrapper">
                <div className="mui-image-wrapper">
                    <img className="mui-image" src={this.props.fileLocalUrl} alt=" "/>
                    <div className="mui-opts">
                        <ButtonCircle icon="fa fa-upload" color="light" onClick={this.props.handleUpload} />
                    </div>
                </div>
            </div>
        </Modal>
    )
}

const mapStateToProps = state => state.filebuffer

export default connect(mapStateToProps)(ModalUploadImage)
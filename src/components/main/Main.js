import React from 'react'
import { postLike, postDislike, postUnlike, postUndislike } from '../../actions';
import { connect } from 'react-redux'
import './index.scss'
import Post from '../post/Post';
import { fetchPosts } from '../../actions/PostActions'

class Main extends React.Component {
    componentDidMount() {
        this.props.dispatch(fetchPosts());
    }

    render = () => (
        <div className="main pb-5 mb-5">
            {this.props.posts.map((post, k) => (
                <Post 
                    key={k}
                    {...post}
                    handleLike={this.props.handleLike}
                    handleDislike={this.props.handleDislike}
                    handleUnlike={this.props.handleUnlike}
                    handleUndislike={this.props.handleUndislike} />
            ))}
        </div>
    )
}

const mapStateToProps = state => state.posts

const mapDispatchToProps = dispatch => ({
    handleLike: id => dispatch(postLike(id)),
    handleDislike: id => dispatch(postDislike(id)),
    handleUnlike: id => dispatch(postUnlike(id)),
    handleUndislike: id => dispatch(postUndislike(id)),
    dispatch: action => dispatch(action)
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);
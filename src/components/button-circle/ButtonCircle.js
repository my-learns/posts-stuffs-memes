import React from 'react'
import './ButtonCircle.scss'
import { Link } from 'react-router-dom'

class ButtonCircle extends React.Component {
    render() {
        let { size, image, color, icon, link, onClick, tooltip } = this.props;
        let className = "";
        let style = {};

        if (size) className += ' b-circle-' + size
        if (color) className += ' b-circle-' + color

        if (tooltip) {
            className += ' utooltip';
            tooltip = <div className="utooltip-text">{tooltip}</div>
        }

        if (image) {
            className += ' b-circle-image';
            style = {
                backgroundImage: `url(${image})`
            }
        }

        if (link) {
            return (
                <Link to={link} className={"b-circle" + className} style={style}>
                    <i className={icon}></i>
                    {tooltip}
                </Link>
            )
        }

        return (
            <div className={"b-circle" + className} style={style} onClick={onClick}>
                <i className={this.props.icon}></i>
                {tooltip}
            </div>
        )
    }
}

export default ButtonCircle;
import React from 'react'
import { connect } from 'react-redux'
import './ModalAdd.scss'
import { 
    // ButtonCircle,
    ButtonUpload 
} from '../btn-circle'
import Modal from '../modal/Modal'
import Hashtag from '../hashtag/Hashtag'
import { 
    postaddHide,
    postaddUpdateDescription,
    postaddRemoveHashtag,
    postaddAddHashtag,
    postaddChangeHashtag,
    postaddSubmit,
    postaddImageUpload
} from '../../actions';
import { 
    postAddFileAssign,
    postAddFileUpload,
    postAddUpload
} from '../../actions/Post'

class ModalAdd extends React.Component {
    constructor(props) {
        super(props);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleHashtagInputChange = this.handleHashtagInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFilePick = this.handleFilePick.bind(this)
        this.submit = this.submit.bind(this)
    }

    handleSubmit(e) {
        e.preventDefault();
        const { dispatch, postAddFileAssigned, postAddFileUploadSuccess, postAddFile } = this.props;
        if (postAddFileAssigned && !postAddFileUploadSuccess) {
            dispatch(postAddFileUpload(postAddFile))
        } else {
            this.submit();
        }
    }

    submit() {
        this.props.dispatch(postAddUpload({
            userId: this.props.userId,
            description: this.props.postAddDescription,
            filename: this.props.postAddFileUploadFilename,
            hashtaghs: this.props.postAddHashtags
        }))
    }

    handleFilePick(file) {
        this.props.dispatch(postAddFileAssign(file))
    }

    componentDidUpdate() {
        const { postAddSuccess, postAddFailed, postAddPending, postAddSubmited, postAddFileUploadPending } = this.props;
        if (!postAddPending && postAddSubmited && !postAddFileUploadPending && !(postAddSuccess || postAddFailed)) {
            this.submit()
        }
    }

    render = () => (
        <Modal active={this.props.active} handleClick={this.props.handleClose}>
            <div className="modal-add-wrapper">
                <div className="modal-add-text-section">
                    <textarea 
                        onInput={e => this.textareaAutogrow(e)}
                        ref="description"
                        value={this.props.postAddDescription} 
                        onChange={() => this.handleDescriptionChange()} 
                        className="modal-add-textarea" 
                        placeholder="Write some description...">
                    </textarea>
                    <div className="d-flex justify-content-center align-items-center modal-add-image">
                        <img 
                            className={"image-fluid img-fluid modal-add-image" + (this.props.postAddFileAssigned ? "" : " d-none")} 
                            src={this.props.postAddFileLocalUrl || ""} 
                            alt={"Epta"}
                            />
                    </div>
                </div>
                <div className="modal-add-section-delimiter d-none"></div>
                <div className="modal-add-hashtag-section d-none">
                    <div className="mb-3">
                        {this.props.postAddHashtags.map((hashtag, k) => (
                            <Hashtag key={k} handleClose={() => this.props.handleRemoveHashtag(hashtag)}>{hashtag}</Hashtag>
                        ))}
                        {!this.props.currentHashtag ? "" :
                        <Hashtag>
                            {this.props.currentHashtag}
                        </Hashtag>}
                    </div>
                    <input 
                        value={this.props.currentHashtag}
                        className="modal-add-hashtag-input"
                        placeholder="Write a hashtag and press enter"
                        onChange={e => this.handleHashtagInputChange(e)}
                        onKeyPress={e => this.handleHashtagInputEnter(e)}
                        type="text"/>
                </div>
                <div className="modal-add-section-delimiter"></div>
                <div className="modal-add-upload-section">
                    <ButtonUpload handleImageUpload={this.handleFilePick}>
                        <i className="far fa-file-image"></i>
                    </ButtonUpload>
                    {/* <ButtonCircle>
                        <i className="far fa-file-video"></i>
                    </ButtonCircle> */}
                    <div className="mr-auto"></div>
                    <span className="d-flex align-items-center">
                        <div>
                            <button onClick={e => this.handleSubmit(e)} className="btn btn-primary">
                                {this.props.postAddFileUploadPending || this.props.postAddPending ? 
                                <div className="spinner-border"></div>
                                :"Post"}
                            </button>
                        </div>
                    </span>
                </div>
                
            </div>
        </Modal>
    )

    textareaAutogrow(e) {
        e.target.style.height = "5px";
        e.target.style.height = (e.target.scrollHeight)+"px";
    }

    handleDescriptionChange() {
        this.props.handleDescriptionChange(this.refs.description.value)
    }

    handleHashtagInputChange(e) {
        this.props.handleChangeHashtag(e.target.value)
    }

    handleHashtagInputEnter = (e) => {
        if (e.key === "Enter") {
            this.props.handleAddHashtag(e.target.value)
        }
    }
}

const mapStateToProps = state => ({
    ...state.addpost,
    userId: state.user.id
})
const mapDispatchToProps = dispatch => ({
    handleClose: () => dispatch(postaddHide()),
    handleDescriptionChange: (text) => dispatch(postaddUpdateDescription(text)),
    handleRemoveHashtag: (text) => dispatch(postaddRemoveHashtag(text)),
    handleAddHashtag: (text) => dispatch(postaddAddHashtag(text)),
    handleChangeHashtag: (text) => dispatch(postaddChangeHashtag(text)),
    handleSubmit: (data) => postaddSubmit(data),
    handleImageUpload: (image, imageUrl) => dispatch(postaddImageUpload(image, imageUrl)),
    dispatch: dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalAdd)
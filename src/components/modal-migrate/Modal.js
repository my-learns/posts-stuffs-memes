import React from 'react'
import "./Modal.scss"
import { connect } from 'react-redux';
import { modalHide } from '../../actions/modal';

class Modal extends React.Component {
    constructor(props) {
        super(props)
        this.handleClickOut = this.handleClickOut.bind(this)
    }

    handleClickOut(e) {
        this.props.dispatch(modalHide())
    }

    render = () => {
        if (this.props.active) {
            document.body.style.overflow = "hidden"
        } else {
            document.body.style.overflow = "unset"
        }
    
        return (
            <div 
                className={"cover-wrapper " + (this.props.active ? "" : " d-none")} 
                onClick={this.handleClickOut}>
                <div 
                    className={"cover" + (this.props.active ? "" : " d-none")} 
                    onClick={this.props.handleClick}>
                    <div className="cover-modal" onClick={e => {e.stopPropagation()}}>
                        {this.props.component}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => state.modal

export default connect(mapStateToProps)(Modal)
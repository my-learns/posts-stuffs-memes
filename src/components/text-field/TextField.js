import React from 'react'
import './TextField.scss'
import ButtonCircle from '../button-circle'

class TextField extends React.Component {
    render() {
        return (
            <div className="text-field-wrapper">
                <div className="text-field-text flex-fill">{this.props.text}</div>
                <div className="field-opts">
                    <ButtonCircle size="sm" icon="fa fa-copy" color="primary" />
                    {/* <Button.Circle.Copy value={this.props.text} onAlert={this.props.onAlert}></Button.Circle.Copy> */}
                </div>
            </div>
        )
    }
}

export default TextField
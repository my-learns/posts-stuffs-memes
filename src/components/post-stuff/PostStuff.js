import React from 'react'
import './PostStuff.scss'
import ButtonCircle from '../button-circle/ButtonCircle'
import TextField from '../text-field'
import { connect } from 'react-redux';
import { meRequestInfo } from '../../actions/User'

class PostStuff extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(meRequestInfo());
    }

    render = () => (
        <div className="post-stuff">
            <div className="post-stuff-header">
                <ButtonCircle image="http://192.168.8.50:8000/img/1590352476690-95C332D9-9CA4-4B54-946B-57D37DAF456A.jpeg"/>
                {/* <ButtonCircle image={this.props} /> */}

                <div className="d-flex flex-column ml-2">
                    <div className="post-stuff-header-title">Some title</div>
                    <div className="text-secondary" style={{lineHeight: ".9rem", fontSize: ".9rem"}}>smoke weed everyday</div>
                </div>
                
                <div className="flex-fill"></div>
                <div className="post-stuff-header-hidden">
                    <ButtonCircle icon="fa fa-arrow-right" color="blue" />
                </div>
            </div>
            {/* <div className={"px-2 pb-3" + (this.props.description ? "" : " d-none")}>{this.props.description}</div> */}
            <div className="post-stuff-body">
                <TextField text="epta"/>
            </div>
            {/* <div className="post-footer">
                footer
            </div> */}
        </div>
    )
}

const mapStateToProps = state => ({
    user: state.user
})

export default connect(mapStateToProps)(PostStuff)
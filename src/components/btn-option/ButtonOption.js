import React from 'react'
import './ButtonOption.scss'
import ButtonCircle from '../button-circle'
import { Link } from 'react-router-dom'

export default class ButtonOption extends React.Component {
    render = () => (
        <Link className="btn-opt" to={this.props.link}>
            <div className="btn-opt-icon">
                <ButtonCircle color={this.props.color ? this.props.color : ""} icon={this.props.icon} image={this.props.image}/>
            </div>
            <div className="btn-opt-text">{this.props.children}</div>
        </Link>
    )
}

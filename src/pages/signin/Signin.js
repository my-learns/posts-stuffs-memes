import React from 'react'
import { connect } from 'react-redux'
import './Signin.scss'
import { 
    Link,
    Redirect
} from 'react-router-dom';
import { authSignin, authPing } from '../../actions/User';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        }
        this.handleUsernameChange = this.handleUsernameChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        document.body.style.backgroundColor = "transparent";
        document.documentElement.style.backgroundColor = "#1488CC23";
        this.props.dispatch(authPing())
    }

    handleUsernameChange(event) {
        const state = this.state;
        state.email = event.target.value;
        this.setState(state);
    }

    handlePasswordChange(event) {
        const state = this.state;
        state.password = event.target.value;
        this.setState(state);
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.email !== "" && this.state.password !== "") {
            this.props.dispatch(authSignin(this.state));
        }
    }

    render() {
        if (this.props.isAuthenticated) {
            return (
                <Redirect to="/"/>
            )
        }

        return (
            <div className="container mt-5 pt-3">
                <div className="row mb-3">
                    <div className="col-md-12">
                        <h1 className="text-center">Sign in</h1>
                        <div className={this.props.authSigninFailed ? "" : "d-none"}>
                            <h6 className="text-center text-danger mb-0">
                                {this.props.authSigninFailedMessage || ""}
                            </h6>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6 mx-auto d-flex justify-content-center">
                        <form method="post" onSubmit={e => this.handleSubmit(e)} className="signin-wrapper">
                            <input
                                name="email"
                                type="text" 
                                className="signin-input" 
                                onChange={this.handleUsernameChange} 
                                value={this.state.username} 
                                placeholder="Email"
                                />
                            <input 
                                name="password"
                                type="password" 
                                className="signin-input mt-2" 
                                onChange={this.handlePasswordChange} 
                                value={this.state.password} 
                                placeholder="Password"
                                />
                            <button className="btn btn-block btn-lg btn-primary mt-3">
                                {this.props.authSigninPending ? <div className="spinner-border"></div> : "Sign in"}
                            </button>
                            <div className="text-center mt-2">
                                Don't have an account? <Link to="/signup">Sign up</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => state.user;

export default connect(mapStateToProps)(Login)
import React from 'react'
import Topbar from '../../components/topbar'
import ButtonOption from '../../components/btn-option'
import PostStuff from '../../components/post-stuff'
import { connect } from 'react-redux';

class Stuff extends React.Component {
    componentDidMount() {
        document.documentElement.style.backgroundColor = "#f2f2f2";
        document.body.style.backgroundColor = "#f2f2f2";
    }

    render = () => (
        <div>
            <Topbar />

            <div className="container">
                <div className="row pt-3">
                    <div className="col-md-3 border-right">
                        <ButtonOption color="blue" icon="fa fa-history">Recent</ButtonOption>
                        <ButtonOption color="blue" icon="fa fa-link">Links</ButtonOption>
                        <ButtonOption color="blue" icon="fa fa-share">Shared</ButtonOption>
                    </div>
                    <div className="col-md-9">
                        <PostStuff />
                    </div>
                </div>
            </div>
        </div>
    )
}



export default connect()(Stuff);
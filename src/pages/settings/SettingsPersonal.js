import React from 'react'
import Field from '../../components/field'
import './Settings.scss'
import ButtonOption from '../../components/btn-option'
import Upload from '../../components/upload'
import ModalUploadImage from '../../components/modal-upload-image'
import { connect } from 'react-redux'
import { settsProfileImageUpload } from '../../actions/Settings'
import { meRequestInfo } from '../../actions/User'

class SettingsPersonal extends React.Component {
    constructor(props) {
        super(props)
        this.handleProfileImageChange = this.handleProfileImageChange.bind(this)
        this.handleProfileImageUpload = this.handleProfileImageUpload.bind(this)
        this.state = {
            modal: false
        }
    }

    componentDidMount() {
        this.props.dispatch(meRequestInfo())
    }

    handleProfileImageChange() {
        this.setState({modal: !this.state.modal})
    }

    handleProfileImageUpload() {
        // console.log("Epta")
        this.props.dispatch(settsProfileImageUpload(this.props.file))
        this.handleProfileImageChange()
    }

    render = () => (
        <div className="sub-setts-container">
            <Upload handleFilePick={this.handleProfileImageChange}>
                <ButtonOption image={this.props.profileImageUrl}>Change picture</ButtonOption>
            </Upload>
            <div className='delimiter'></div>
            <Field label="Firstname" name="firstname" />
            <div className='delimiter'></div>
            <Field label="Lastname" name="lastname" />
            <div className='delimiter'></div>
            {/* <Field label="Nickname" name="nickname" /> */}
            <ModalUploadImage 
                active={this.state.modal}
                handleHide={this.handleProfileImageChange}
                handleUpload={this.handleProfileImageUpload}
                />
    </div>
    )
}

const mapStateToProps = state => ({
    ...state.filebuffer,
    profileImageUrl: state.user.profileImageUrl
})

export default connect(mapStateToProps)(SettingsPersonal)
import React from 'react'
// import ButtonOption from '../../components/btn-option/ButtonOption'
import './Settings.scss'
import ButtonCircle from '../../components/button-circle';
// import Field from '../../components/field';
import { connect } from 'react-redux';
// import { settsFieldUpdate } from '../../actions/Settings';
import MediaQuery from 'react-responsive'
// import { useMediaQuery } from 'react-responsive'
import { Route, Switch } from 'react-router-dom';
import SettingsPersonal from './SettingsPersonal';
import SettingsMenu from './SettingsMenu';

class Settings extends React.Component {
    // componentDidMount() {
    //     document.documentElement.style.backgroundColor = "#f2f2f2";
    //     document.body.style.backgroundColor = "#f2f2f2";
    // }

    render = () => {
        document.documentElement.style.backgroundColor = "#f2f2f2";
        document.body.style.backgroundColor = "#f2f2f2";
        return (
            <div>
                <MediaQuery minDeviceWidth={1224}>
                    <SettingsDesktop />
                </MediaQuery>
                <MediaQuery maxDeviceWidth={1224}>
                    <SettingsMobile />
                </MediaQuery>
            </div>
        )
    }
}


const SettingsDesktop = (props) => (
    <div>
        <div className='s-topbar p-3'>
            <ButtonCircle link="/" color="light" icon="fa fa-arrow-left" />
        </div>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="row setts-container">
                        <div className="col-md-3 border-right">
                            <SettingsMenu />
                        </div>
                        <div className="col-md-9 r-pads sub-setts-container">
                            {/* <Route path="/settings/personal"> */}
                                <SettingsPersonal />
                            {/* </Route> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

const SettingsMobile = (props) => (
    <div>
        <div className='s-topbar p-3'>
            <div className="s-topbar-flex">
                <Switch>
                    <Route path="/settings/personal">
                        <ButtonCircle link="/settings" color="light" icon="fa fa-arrow-left" />
                        <div className="s-topbar-title">Personal info</div>
                    </Route>
                    <Route path="/settings/security">
                        <ButtonCircle link="/settings" color="light" icon="fa fa-arrow-left" />
                        <div className="s-topbar-title">Account security</div>
                    </Route>
                    <Route path="/settings">
                        <ButtonCircle link="/" color="light" icon="fa fa-arrow-left" />
                        <div className="s-topbar-title">Settings</div>
                    </Route>
                </Switch>
            </div>
        </div>
        <div className="p-3 pt-3 setts-container">
            <Switch>
                <Route path="/settings/personal">
                    <SettingsPersonal />
                </Route>
                <Route path="/settings/security">
                    <div>security</div>
                </Route>
                <Route path="/settings">
                    <SettingsMenu />
                </Route>
            </Switch>
        </div>
    </div>
)



const mapStateToProps = state => state.settings

export default connect(mapStateToProps)(Settings);
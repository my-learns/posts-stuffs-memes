import React from 'react'
import ButtonOption from '../../components/btn-option'

const SettingsMenu = (props) => (
    <div className="containerj">
        <ButtonOption link="/settings/personal" color="blue" icon="fa fa-user">Personal</ButtonOption>
        <div className="delimiter"></div>
        <ButtonOption link="/settings/security" color="blue" icon="fa fa-shield-alt">Security</ButtonOption>
        <div className="delimiter"></div>
        <ButtonOption icon="fa fa-times" color="close">Sign out</ButtonOption>
    </div>
)

export default SettingsMenu
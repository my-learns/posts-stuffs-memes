import React from 'react'
import { connect } from 'react-redux'
import ButtonCircle from '../../components/button-circle';

import { useParams } from 'react-router-dom'
import { quizGet } from '../../actions/Quiz';

const Quiz = (props) => {
    let { quizId } = useParams();

    return <QuizPage id={quizId} {...props}/>
}


class QuizPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(quizGet(this.props.id))
    }

    render() {
        return (
            <div>
                <div className='s-topbar p-3'>
                    <ButtonCircle link="/" color="light" icon="fa fa-arrow-left" />
                </div>
                <div>
                    {this.props.name}
                </div>
            </div>
        )
    }
}


const mapStatetoProps = state => ({
    ...state.quiz
})

export default connect(mapStatetoProps)(Quiz)
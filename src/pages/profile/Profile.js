import React from 'react'
import './Profile.scss'

import { useParams } from 'react-router-dom'
import { connect } from 'react-redux'
import ButtonCircle from '../../components/button-circle'
import { userProfile } from '../../actions/User'

const Profile = (props) => {
    let { userReference } = useParams();
    return <ProfilePage userRef={userReference} {...props}/>
}

class ProfilePage extends React.Component {
    componentDidMount() {
        document.body.style.backgroundColor = "transparent";
        document.documentElement.style.backgroundColor = "#f2f2f2";
        this.props.dispatch(userProfile(this.props.userRef))
    }

    render = () => {
        
        return (
            <div className="profile-wrapper">
                <div className="profile-header">
                    <div className="profile-topbar">
                        <ButtonCircle color="light" link="/" icon="fa fa-arrow-left" />
                    </div>

                    <div className="profile-cover"></div>

                    <div className="profile-user-header">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="d-flex w-100">
                                        <div>
                                            <div className="profile-user-image" style={{backgroundImage: "url(" + this.props.profileImageUrl + ")"}}></div>
                                        </div>
                                        <div className="d-flex flex-column ml-3 flex-fill">
                                            <div className="flex-fill d-flex flex-column pb-3">
                                                <div className="flex-fill"></div>
                                                <div className="">
                                                    <h3 className="mb-0" style={{whiteSpace: "nowmrap"}}>
                                                        {this.props.firstname + " " + this.props.lastname}
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="flex-fill">
                                                <div className="containeri">
                                                    <div className="row">
                                                        <div className="col-md-4 pb-3">36.5k followers</div>
                                                        <div className="col-md-2 ml-auto">
                                                            <button className="btn btn-block btn-outline-primary">Follow</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => state.profile

export default connect(mapStateToProps)(Profile)
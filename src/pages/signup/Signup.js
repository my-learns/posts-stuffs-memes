import React from 'react'
import { connect } from 'react-redux'
import './Signup.scss'
import { 
    authSignup,
    authPing
} from '../../actions/User';
import { 
    Redirect,
    Link
} from 'react-router-dom';

class Signup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            password: "",
            password2: "",
            nickname: "",
            passwordValidationError: "",
        }

        this.handleChangeFirstname = this.handleChangeFirstname.bind(this)
        this.handleChangeLastaname = this.handleChangeLastname.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleChangePassword2 = this.handleChangePassword2.bind(this)
        this.handleChangeNickname = this.handleChangeNickname.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        document.body.style.backgroundColor = "transparent";
        document.documentElement.style.backgroundColor = "#1488CC23";
        // document.documentElement.style.backgroundColor = "#f2f2f2";
        this.props.dispatch(authPing())
    }

    handleChangeFirstname(e) {
        e.preventDefault();
        this.setState(Object.assign({}, this.state, {
            firstname: e.target.value
        }))
    }

    handleChangeLastname(e) {
        e.preventDefault();
        this.setState(Object.assign({}, this.state, {
            lastname: e.target.value
        }))
    }

    handleChangeEmail(e) {
        e.preventDefault();
        this.setState(Object.assign({}, this.state, {
            email: e.target.value
        }))
    }

    handleChangePassword(e) {
        e.preventDefault();
        this.setState(Object.assign({}, this.state, {
            password: e.target.value,
            passwordValidationError: ""
        }))
    }

    handleChangePassword2(e) {
        e.preventDefault();
        this.setState(Object.assign({}, this.state, {
            password2: e.target.value,
            passwordValidationError: ""
        }))
    }

    handleChangeNickname(e) {
        e.preventDefault();
        this.setState(Object.assign({}, this.state, {
            nickname: e.target.value
        }))
    }

    handleSubmit(e) {
        e.preventDefault();

        if (this.state.password !== this.state.password2) {
            this.setState(Object.assign({}, this.state, {
                passwordValidationError: "Password must be identical"
            }))
            return;
        }

        this.props.dispatch(authSignup(this.state))
    }

    render = () => {
        if (this.props.authSignupSuccess || this.props.isAuthenticated) {
            return <Redirect to="/"/>
        }

        return (
            <div className="container mt-5">
                <div className="row mb-3">
                    <div className="col-md-12">
                        <h1 className="text-center">Sign up</h1>
                        <div className={this.props.authSignupFailed ? "" : "d-none"}>
                            <h6 className="text-center text-danger mb-0">
                                {this.props.authSignupFailedMessage || ""}
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 mx-auto d-flex justify-content-center">
                        <form method="post" className="signup-wrapper" onSubmit={e => this.handleSubmit(e)}>
                            <div>
                                <label>Personal info</label>
                                <input 
                                    name="firstname" 
                                    type="text" 
                                    placeholder="First name" 
                                    onChange={e => this.handleChangeFirstname(e)}
                                    value={this.state.firstname}
                                    required
                                    className="signup-input"
                                    />
                                <input 
                                    name="lastname" 
                                    type="text" 
                                    placeholder="Last name" 
                                    onChange={e => this.handleChangeLastaname(e)}
                                    value={this.state.lastname} 
                                    className="mt-2 signup-input"
                                    />
                            </div>

                            <div>
                                <label>Contact/verification</label>
                                <input 
                                    name="email" 
                                    type="email" 
                                    placeholder="Email address" 
                                    onChange={e => this.handleChangeEmail(e)}
                                    value={this.state.email}
                                    title="Type a valid email address"
                                    required
                                    className="signup-input"
                                    />
                            </div>

                            <div>
                                <label className={this.state.passwordValidationError ? "text-danger" : ""}>
                                    {this.state.passwordValidationError || "Security"}
                                </label>
                                <input 
                                    name="password" 
                                    type="password" 
                                    placeholder="Password" 
                                    onChange={e => this.handleChangePassword(e)}
                                    value={this.state.password}
                                    pattern="^.{8,100}$"
                                    required
                                    title="Password must have minimum 8 characters"
                                    className="signup-input"
                                    />
                                <input 
                                    name="password2" 
                                    type="password" 
                                    placeholder="Repeat password"
                                    onChange={e => this.handleChangePassword2(e)}
                                    value={this.state.password2}
                                    pattern="^.{8,100}$"
                                    required
                                    title="Password must have minimum 8 characters"
                                    className="mt-2 signup-input"
                                    />
                            </div>

                            <div>
                                <label>Make a nickname</label>
                                <input 
                                    name="nickname" 
                                    type="text" 
                                    placeholder="Nickname" 
                                    onChange={e => this.handleChangeNickname(e)}
                                    value={this.state.nickname}
                                    className="signup-input"
                                    />
                            </div>

                            <div>
                                <button className="btn btn-lg btn-block btn-primary">
                                    {this.props.authSignupPending ? <span className="spinner-border"></span> : "Sign up"}
                                </button>
                            </div>

                            <div className="d-flex justify-content-center">
                                <h6 className="font-weight-light mb-0">
                                    Already have an account? <Link to="/signin">Sign in</Link>
                                </h6>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => state.user

export default connect(mapStateToProps)(Signup)
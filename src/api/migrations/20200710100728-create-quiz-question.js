'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('QuizQuestions', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			quizId: {
				type: Sequelize.INTEGER
			},
			text: {
				type: Sequelize.STRING
			},
			answer1: {
				type: Sequelize.STRING
			},
			answer2: {
				type: Sequelize.STRING
			},
			answer3: {
				type: Sequelize.STRING
			},
			correctAnswer: {
				type: Sequelize.INTEGER
			},
			active: {
				type: Sequelize.BOOLEAN
			},
			createdBy: {
				type: Sequelize.INTEGER
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
			deletedAt: {
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('QuizQuestions');
	}
};
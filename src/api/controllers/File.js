const models = require("../models");
const axios = require('axios');
const fs = require('fs');

module.exports = {
    download: async (req, res) => {
        const matchTagLink = /<link\s*(rel="[^"]*icon[^"]*"\s*href="([^"]*)"|href="([^"]*)"\s*rel="[^"]*icon[^"]*")\s*>/
        const matchDomain = /((http:\/\/|https:\/\/)[^\/]*)/
        const matchFileExt = /\.(ico|jpeg|jpg|png)/

        let domain;
        let url;
        let reserveUrl;
        let ext;
        let preg = req.body.url.match(matchDomain);
        
        if (!preg) {
            res.status(200).json({ok: false, message: "Wrong url"});
            return;
        }

        domain = preg[0];

        const result = await models.Favicon.findOne({
            where: {
                domain: domain
            }
        })
        
        if (result !== null) {
            res.status(200).json({ok: true, fileUrl: req.reqUrl + "/img/" + result.filename});
            return;
        }

        let response = await axios.get(req.body.url).catch((err) => res.status(200).json({ok: false}))
        
        preg = response.data.match(matchTagLink);

        if (preg) {
            url = preg[2] || preg[3];
            reserveUrl = domain + '/favicon.ico';
        } else {
            preg = req.body.url.match(matchDomain);

            if (preg) {
                url = preg[0] + "/favicon.ico";
            }
        }

        preg = url.match(matchFileExt)

        if (preg && url) {
            ext = preg[1];
            const filename =  Date.now() + '.' + ext
            const path = req.basePath + '\\res\\' + filename;
            const file = fs.createWriteStream(path);

            console.log(url);

            response = await axios.get(url, {responseType: 'stream'}).catch((err) => {})

            if (!response) {
                response = await axios.get(reserveUrl, {responseType: 'stream'}).catch((err) => {})
            }

            response.data.pipe(file);

            models.Favicon.create({
                domain: domain,
                filename: filename
            })
            res.status(200).json({ok: true, fileUrl: req.reqUrl + "/img/" + filename});
        } else {
            res.status(200).json({ok: false})
        }
	}
}
const models = require("../models");
const jwt = require('jsonwebtoken');

module.exports = {
    get(req, res) {
        models.User
            .findByPk(req.params.userId)
            .then((user) => (res.status(200).json({
                id: user.id,
                nickname: user.nickname,
                firstname: user.firstname,
                lastname: user.lastname,
                profileImageUrl: "http://localhost:8000/img/" + user.profileImageFilename
            })))
            .catch((err) => console.log(err))
    },

    create(req, res) {
        models.User
            .create({
                nickname: req.data.nickname,
                email: req.data.email,
                firstname: req.data.firsname,
                lastname: req.data.lastname,
                profileImageFilename: req.data.profileImageFilename,
                password: req.data.password,
                roleId: 1
            })
            .then((user) => res.status(200).json({id: user.id}))
            .catch((err) => console.log(err))
    },

    login(req, res) {
        models.User.findOne({
            where: {
                nickname: req.body.username,
                password: req.body.password
            }
        })
        .then((user) => {
            if (user) {
                const token = jwt.sign({id: user.id, role: user.roleId}, process.env.TOKEN_SECRET);
                res.status(200).json({ok: true, token: token})
            } else {
                res.status(200).json({ok: false, message: "Wrong credentials"});
            }
        })
        .catch((err) => res.status(200).json({ok: false, message: "Some error, try refresh"}))
    },

    loginCheck(req, res) {
        res.status(200).json({ok: true})
    },

    signout(req, res) {
        res.status(200).json({ok: true})
    },

    getData(req, res) {
        models.User.findByPk(req.user.id)
        .then((user) => {
            res.status(200).json({
                ok: true,
                user: {
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    profileImageUrl: req.reqUrl + '/img/' + user.profileImageFilename,
                }
            })
        })
    },

    getInfo(req, res) {
        models.User.findByPk(req.params.userRef)
            .then((user) => {
                if (user) {
                    res.status(200).json({
                        ok: true,
                        user: {
                            firstname: user.firstname,
                            lastname: user.lastname,
                            profileImageUrl: req.reqUrl + "/img/" + user.profileImageFilename
                        }
                    })
                } else {
                    res.status(200).json({ok: false})
                }
            })
            .catch((err) => res.status(400).json({ok: false}))
    },

    setProfileImage(req, res) {
        var filename = Date.now();
        console.log("Epta");
        var multer = require('multer');
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, 'res')
            },
            filename: function (req, file, cb) {
                filename += "-" + file.originalname;
                cb(null, filename);
            }
        })
        var upload = multer({ storage: storage }).single('file')
        
        upload(req, res, async function (err) {
            if (err instanceof multer.MulterError) {
                return res.status(500).json(err)
            } else if (err) {
                return res.status(500).json(err)
            }
            
            let user = await models.User.findByPk(req.user.id)

            console.log(user);

            user.profileImageFilename = filename;
            await user.save();

            return res.status(200).json({ok: true, filename: filename})
        })
    }
}
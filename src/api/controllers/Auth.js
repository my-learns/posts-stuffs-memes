const models = require("../models");
const jwt = require('jsonwebtoken');

module.exports = {
    signin: (req, res) => {
        models.User.findOne({
            where: {
                email: req.body.email,
                password: req.body.password
            }
        })
        .then((user) => {
            if (user) {
                const token = jwt.sign({id: user.id, role: user.roleId}, process.env.TOKEN_SECRET);
                res.status(200).json({ok: true, token: token})
            } else {
                res.status(200).json({ok: false, message: "Wrong credentials"});
            }
        })
        .catch((err) => res.status(200).json({ok: false, message: "Internal error, try refresh"}))
    },

    signup: async (req, res) => {
        const { email, password, firstname } = req.body;

        if (!(email && password && firstname)) {
            res.status(400).send()
        } else {
            let error = false;
            let nicknameCheck;
            let emailCheck = models.User.findOne({
                where: {
                    email: email
                }
            })
            .then((user1) => {
                if (user1) {
                    res.status(200).json({ok: false, message: "Email already registered"});
                    return false;
                }
                return true;
            })

            if (req.body.nickname && await emailCheck) {
                nicknameCheck = models.User.findOne({
                    where: {
                        nickname: req.body.nickname
                    }
                })
                .then((user2) => {
                    if (user2) {
                        res.status(200).json({ok: false, message: "Nickname already taken"});
                        return false;
                    }
                    return true;
                })
            }

            if (await emailCheck && await nicknameCheck) {
                models.User.create({
                    firstname: firstname,
                    lastname: req.body.lastname,
                    email: email,
                    password: password,
                    nickname: req.body.nickname,
                    roleId: 2
                }).then((user) => {
                    if (user) {
                        const token = jwt.sign({id: user.id, role: user.roleId}, process.env.TOKEN_SECRET);
                        res.status(200).json({ok: true, token: token})
                    } else {
                        res.status(200).json({ok: false, message: "Internal error"})
                    }
                })
            }
        }
    },

    signout: (req, res) => {
        res.status(200).json({ok: true})
    },

    checkIfLoggedIn: (req, res) => {
        res.status(200).json({ok: true})
    }
}
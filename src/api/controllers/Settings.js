var models = require('../models');

module.exports = {
    get: (req, res) => {
        models.User.findByPk(req.user.id)
        .then((user) => {
            res.status(200).json({value: user[req.params.fieldName]})
        })
    },

    set: async (req, res) => {
        let user = await models.User.findByPk(req.user.id);
        user[req.params.fieldName] = req.body.value;
        await user.save();

        res.status(200).json({ok: true})
    }
}
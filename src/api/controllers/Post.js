const models = require("../models");
const Sequelize = require('sequelize');

module.exports = {
    create: (req, res) => {
        models.Image.create({
            filename: req.body.image
        })
        .then((image) => {
            models.Post.create({
                userId: 1,
                text: req.body.description,
                img: image.id
            })
            .then((post) => {
                req.body.hashtags.map((hashtag) => {
                    models.Hashtag.findOrCreate({
                        where: {
                            text: hashtag
                        }
                    })
                    .then(([insertedHashtag, created]) => {
                        models.PostHashtag.create({
                            postId: post.id,
                            hashtagId: insertedHashtag.id
                        })
                        .then((postHashtag) => res.status(200).send())
                        .catch((err) => res.status(400).send(err))
                    })
                    .catch((err) => res.status(400).send())
                })
            })
            .catch((err) => res.status(400).send(err));
        })
        .catch((err) => res.status(400).send(err));
    },

    add: (req, res) => {
        const { userId, description, filename } = req.body;
        models.Post.create({
            userId: userId,
            description: description,
            filename: filename
        })
        .then((post) => {
            res.status(200).json({ok: true, post: post});
        })
        .catch((err) => {res.status(200).json({ok: false})})
    },

    getAllWithUsers: async (req, res) => {
        let posts = await models.Post.findAll({
            include: [
                {
                    model: models.User,
                    as: 'User'
                },
                // {
                //     model: models.Like,
                //     as: 'Like'
                // }
            ],

            order: [
                ['createdAt', 'DESC']
            ]
        })
        .then((posts) =>  posts.map((post) => ({
                id: post.id,
                description: post.description,
                fileUrl: req.reqUrl + '/img/' + post.filename,
                likes: post.likes,
                created: post.createdAt,
                userId: post.User.id,
                userFullname: post.User.firstname + ' ' + post.User.lastname,
                userProfileImageUrl: req.reqUrl + '/img/' + post.User.profileImageFilename
            }))
        )
        .catch((err) => res.status(200).send(err))

        if (req.user) {
            let likes = await models.Like.findAll({
                where: {
                    userId: req.user.id,
                    postId: {
                        [Sequelize.Op.in]: posts.map((post) => (post.id))
                    }
                }
            })

            posts = posts.map((post) => {
                return {
                    ...post,
                    liked: likes.filter((like) => like.postId == post.id).length ? true : false
                }
            })

            res.status(200).json({ok:true, posts: posts});
        } else {
            res.status(200).json({ok:true, posts: posts});
        }
    },

    uploadImage(req, res) {
        var filename = Date.now();

        var multer = require('multer');
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, 'res')
            },
            filename: function (req, file, cb) {
                filename += "-" + file.originalname;
                cb(null, filename);
            }
        })
        var upload = multer({ storage: storage }).single('file')
        
        upload(req, res, function (err) {
            if (err instanceof multer.MulterError) {
                return res.status(500).json(err)
            } else if (err) {
                return res.status(500).json(err)
            }
            // console.log(filename);
            return res.status(200).json({ok: true, filename: filename})
        })
    },

    getAll(req, res) {
        // models.Post.findAll({
        //     include: [{
        //         model: models.Image,
        //         as: 'Image',
        //     }],
        // })
        // .then((posts) => {
        //     const psts = posts.map((post) => ({
        //         user_ref: 1,
        //         user_name: "Jora Baton",
        //         user_img: req.protocol + '://' + req.get('host') + '/img/avatar.jpg',
        //         post_datetime: "Miercuri, 13:49",
        //         post_img: req.protocol + '://' + req.get('host') + '/img/' + post.Image.filename,
        //         // post_img: `http://localhost:8000/img/` + post.Image.filename,
        //         likes: 2233,
        //         dislikes: 117,
        //         liked: true,
        //         disliked: false,

        //         postId: post.id,
        //         // id: post.id,
        //         description: post.text,
        //         createdAt: post.createdAt,
        //         image: post.Image.filename
        //     }))
        //     res.status(200).json(psts);
        // })
        // .catch((err) => res.status(400).send(err))
    },

    getImage(req, res) {
        console.log(req.get('host'));
        var path = require('path');
        res.status(200).sendFile(path.resolve('res/' + req.params.filename));
    },

    getHashtags(req, res) {
        models.PostHashtag.findAll({
            include: [{
                model: models.Hashtag,
                as: "Hashtag",
            }],
            where: {
                postId: req.params.postId
            }
        })
        .then((postHashtags) => (res.status(200).json(postHashtags.map((postHashtag) => (postHashtag.Hashtag.text)))))
        .catch((err) => console.log(err))
    },

    test: (req, res) => {
        console.log(req.data);
        res.status(200).json({ok: true})
    },

    like: async (req, res) => {
        let userId = req.user.id;
        let postId = req.params.postId;
        // let t = await models.sequelize.transaction();

        try {

            let [like, created] = await models.Like.findOrCreate({
                where: {
                    userId: userId,
                    postId: postId
                },
                defaults: {
                    userId: userId,
                    postId: postId
                }
            })

            console.log("----------Epta" + created);

            let post = await models.Post.findByPk(postId)

            if (created) {
                post.likes = post.likes + 1;
                await post.save();
            }


            // await t.commit();

            res.status(200).json({ok: true, likes: post.likes})
        } catch (error) {
            // await t.rollback();
            res.status(200).json({ok: false, message: error.message})
        }
    }
}
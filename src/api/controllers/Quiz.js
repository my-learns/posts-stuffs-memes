const sequelize = require('sequelize')
const models = require('../models')

module.exports = {
    getAll: async (req, res) => {
        const quizzes = await models.Quiz.findAll({
            attributes: [
                'id',
                'name',
                [sequelize.fn('CONCAT', 'http://127.0.0.1:8000', sequelize.col('imageFilename')), 'imageUrl']
            ]
        })

        res.status(200).json(quizzes)
    },

    getById: async (req, res) => {
        const quizId = req.params.quizId || 0

        const quiz = await models.Quiz.findByPk(quizId, {
            attributes: [
                'id',
                'name',
                'imageFilename'
            ]
        })

        res.status(200).json(quiz)
    },

    getQuestionsByQuizId: async (req, res) => {
        const quizId = req.params.quizId || 0

        const questions = await models.QuizQuestion.findAll({
            attributes: [
                'id',
                'text',
                'answer1',
                'answer2',
                'answer3',
                'correctAnswer'
            ],
            where: {
                quizId: quizId
            }
        })

        res.status(200).json(questions)
    },

    getQuestionByQuizIdAndOffset: async (req, res) => {
        const quizId = req.params.quizId || 0
        const offset = parseInt(req.params.offset) - 1 || 0

        const question = await models.QuizQuestion.findOne({
            attributes: [
                'id',
                'text',
                'answer1',
                'answer2',
                'answer3',
                'correctAnswer'
            ],
            where: {
                quizId: quizId
            },
            offset: offset,
            limit: 1
        })

        res.status(200).json(question)
    },

    test: async (req, res) => {

        const quiz = await models.QuizQuestion.findByPk(310)

        quiz.destroy()
        // const quiz = await models.QuizQuestion.create({
        //     text: "test",
        //     active: true,
        //     // imageFilename: "url",
        //     createdBy: 1
        // })

        res.status(200).json(quiz)
    }
}
const Post = require('./Post');
const User = require('./User');
const Auth = require('./Auth');
const Settings = require('./Settings');
const File = require('./File');
const Quiz = require('./Quiz')

module.exports = {
  Post,
  User,
  Auth,
  Settings,
  File,
  Quiz
};
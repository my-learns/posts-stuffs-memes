'use strict';
module.exports = (sequelize, DataTypes) => {
  const Resource = sequelize.define('Resource', {
    type: DataTypes.INTEGER,
    filename: DataTypes.STRING
  }, {});
  Resource.associate = function(models) {
    // associations can be defined here
  };
  return Resource;
};
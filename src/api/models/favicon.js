'use strict';
module.exports = (sequelize, DataTypes) => {
  const Favicon = sequelize.define('Favicon', {
    domain: DataTypes.STRING,
    filename: DataTypes.STRING
  }, {});
  Favicon.associate = function(models) {
    // associations can be defined here
  };
  return Favicon;
};
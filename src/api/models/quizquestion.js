'use strict';
module.exports = (sequelize, DataTypes) => {
	const QuizQuestion = sequelize.define('QuizQuestion', {
		quizId: DataTypes.INTEGER,
		text: DataTypes.STRING,
		answer1: DataTypes.STRING,
		answer2: DataTypes.STRING,
		answer3: DataTypes.STRING,
		correctAnswer: DataTypes.INTEGER,
		active: DataTypes.BOOLEAN,
		createdBy: DataTypes.INTEGER
	}, {
		paranoid: true
	});
	QuizQuestion.associate = function(models) {
		// associations can be defined here
	};
	return QuizQuestion;
};
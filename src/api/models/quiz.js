'use strict';
module.exports = (sequelize, DataTypes) => {
	const Quiz = sequelize.define('Quiz', {
		name: DataTypes.STRING,
		active: DataTypes.BOOLEAN,
		imageFilename: DataTypes.STRING,
		createdBy: DataTypes.INTEGER
	}, {
		paranoid: true
	});
	Quiz.associate = function(models) {
		// associations can be defined here
	};
	return Quiz;
};
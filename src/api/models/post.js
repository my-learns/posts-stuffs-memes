'use strict';
module.exports = (sequelize, DataTypes) => {
	const Post = sequelize.define('Post', {
		userId: DataTypes.INTEGER,
		description: DataTypes.STRING,
		filename: DataTypes.STRING,
		likes: DataTypes.INTEGER
	}, {});
	Post.associate = function(models) {
		Post.belongsTo(models.User, {
			foreignKey: 'userId'
		})

		Post.hasMany(models.Like, {
			foreignKey: 'postId'
		})
	};
	return Post;
};
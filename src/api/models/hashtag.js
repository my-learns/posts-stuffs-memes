'use strict';
module.exports = (sequelize, DataTypes) => {
	const Hashtag = sequelize.define('Hashtag', {
		text: DataTypes.STRING
	}, {});
	Hashtag.associate = function(models) {
		Hashtag.hasMany(models.PostHashtag, {
			foreignKey: "hashtagId",
			as: "PostHashtag"
		})
	};
	return Hashtag;
};
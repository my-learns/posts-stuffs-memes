'use strict';
module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		nickname: DataTypes.STRING,
		email: DataTypes.STRING,
		firstname: DataTypes.STRING,
		lastname: DataTypes.STRING,
		password: DataTypes.STRING,
		phoneNumber: DataTypes.STRING,
		profileImageFilename: DataTypes.STRING,
		roleId: DataTypes.INTEGER
	}, {});
	User.associate = function(models) {
		User.hasMany(models.Post, {
			foreignKey: 'userId'
		})
	};
	return User;
};
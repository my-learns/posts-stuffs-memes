'use strict';
module.exports = (sequelize, DataTypes) => {
	const PostHashtag = sequelize.define('PostHashtag', {
		postId: DataTypes.INTEGER,
		hashtagId: DataTypes.INTEGER
	}, {});
	PostHashtag.associate = function(models) {
		// PostHashtag.belongsTo(models.Hashtag, {
		// 	as: "Hashtag"
		// })
	};
	return PostHashtag;
};
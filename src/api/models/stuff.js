'use strict';
module.exports = (sequelize, DataTypes) => {
  const Stuff = sequelize.define('Stuff', {
    title: DataTypes.STRING,
    text: DataTypes.STRING,
    iconFilename: DataTypes.STRING
  }, {});
  Stuff.associate = function(models) {
    // associations can be defined here
  };
  return Stuff;
};
const controllers = require('../controllers');
const models = require('../models');

const Path = require('path') 

module.exports = (app) => {

	// Quiz
    app.get('/quizzes', controllers.Quiz.getAll)
    app.get('/quiz/:quizId', controllers.Quiz.getById)
    app.get('/quiz/:quizId/questions', controllers.Quiz.getQuestionsByQuizId)
    app.get('/quiz/:quizId/question/:offset', controllers.Quiz.getQuestionByQuizIdAndOffset)
    
	app.get('/epta', async (req, res) => {
		console.log("Epta");

		// const user = await models.User.findOne({
		// 	where: {
		// 		email: "saniok",
		// 		password: "saniok2000"
		// 	}
		// })

		console.log("Epta");

		// res.status(200).json(user);
	})

	app.get('/test', async (req, res) => {
		// res.status(200).send(process.env.TOKEN_SECRET)

		console.log("Epta");

		const user = await models.User.findOne({
			where: {
				email: "saniok",
				password: "saniok2000"
			}
		})

		console.log("Epta");

		res.status(200).json(user);
			


	})

	app.get('/api', (req, res) => res.status(200).send({
		message: 'Welcome to the Todos API!',
	}));

	app.get('/insert', controllers.Post.create);

	app.post('/post', controllers.Post.create);
	app.get('/post', controllers.Post.getAll);
	app.get('/post/:postId/hashtags', controllers.Post.getHashtags);

	app.get('/img/:filename', controllers.Post.getImage)

	app.post('/upload', controllers.Post.uploadImage);



	// app.get('/u/:userId', controllers.User.get);
	app.post('/u', controllers.User.create)
	app.get('/auth/signout', controllers.User.signout)
	app.post('/login', controllers.User.login)
	app.get('/login/check', controllers.User.loginCheck)
	app.get('/u.me', controllers.User.getData)

	
	// user
	app.get('/user/:userRef', controllers.User.getInfo);

	// post
	app.post('/post/add', controllers.Post.add);
	app.get('/post/all', controllers.Post.getAllWithUsers);

	app.post('/post/:postId/like', controllers.Post.like);

	// settings
	app.post('/setts/profileimage', controllers.User.setProfileImage)
	app.get('/setts/:fieldName', controllers.Settings.get);
	app.post('/setts/:fieldName', controllers.Settings.set);

	// auth
	app.post('/auth/signin', controllers.Auth.signin)
	app.post('/auth/signup', controllers.Auth.signup)
	app.post('/auth/signout', controllers.Auth.signout)
	app.get('/auth/ping', controllers.Auth.checkIfLoggedIn)


	// utility
	app.post('/req', controllers.File.download)
};
const dotenv = require('dotenv').config()
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require("cors");
// const path = require("path");

const unless = require('express-unless')
const jwt = require('express-jwt')

jwt.unless = unless;

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    // req.url = req.protocol + '://' + req.get('host');
    req.reqUrl = 'http://localhost:8000';
    // req.reqUrl = 'http://192.168.8.50:8000';
    // req.reqUrl = 'http://192.168.0.101:8000';
    req.basePath = __dirname;
    // req.reqUrl = 'http://192.168.89.188:8000';
    next();
})

app.use(jwt({
    secret: process.env.TOKEN_SECRET,
}).unless({
    path: [
        '/login', 
        /\/img\/.*/, 
        '/post', 
        /\/post\/\d*\/hashtags/,
        /\/auth\/(signin|signout|signup)/,
        /\/u\/.*/,
        /\/user\/\d{1,10}/,
        // '/post/all',
        '/test'
    ]
}))

app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
      res.status(200).json({ok: false});
    }
});

// Require our routes into the application.




require('./routes')(app);


module.exports = app;
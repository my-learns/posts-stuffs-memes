import React from 'react';
import ReactDOM from 'react-dom';
import { useParams } from 'react-router-dom'
import App from './App';
// import {
// 	BrowserRouter as Router,
// 	Switch,
// 	Route,
// } from "react-router-dom";

import URouter from './Router'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import rootReducer from './reducers'

import axios from 'axios'
import Signin from './pages/signin';
import Signup from './pages/signup';
import Profile from './pages/profile';
import Settings from './pages/settings';
import Stuff from './pages/stuffs';
import Modal from './components/modal-migrate';
import Quiz from './pages/quiz';
axios.defaults.baseURL = 'http://localhost:8000';
// axios.defaults.baseURL = 'http://192.168.8.50:8000';
// axios.defaults.baseURL = 'http://192.168.0.101:8000';
// axios.defaults.baseURL = 'http://192.168.89.188:8000';

console.log(localStorage.token)

if (localStorage.token) {
	axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token
}

console.log(axios.defaults.headers.common['Authorization']);

const store = createStore(
	rootReducer,
	applyMiddleware(
	  	thunkMiddleware, // lets us dispatch() functions
	  	// loggerMiddleware // neat middleware that logs actions
	)
)

ReactDOM.render(
	<Provider store={store}>
		<URouter />
			{/* <Switch>
				<Route path="/stuffs">
					<Stuff />
				</Route>
				<Route path="/u/:userReference">
					<Profile />
				</Route>
				<Route path="/settings">
					<Settings />
				</Route>
				<Route path="/signin">
					<Signin />
				</Route>
				<Route path="/signup">
					<Signup />
				</Route>
				<Route path="/quiz/:quizId">
					<Quiz id={useParams()}/>
				</Route>
				<Route path="/">
					<App />
				</Route>
			</Switch>
			<Modal /> */}
		{/* </URouter> */}
	</Provider>,
	document.getElementById('root')
);
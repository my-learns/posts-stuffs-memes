import React from 'react'
import { useParams } from 'react-router-dom'
import App from './App';
import {
	BrowserRouter as Router,
	Switch,
	Route,
} from "react-router-dom";

import Signin from './pages/signin';
import Signup from './pages/signup';
import Profile from './pages/profile';
import Settings from './pages/settings';
import Stuff from './pages/stuffs';
import Modal from './components/modal-migrate';
import Quiz from './pages/quiz';


const URouter = (props) => {
    
    // const params = useParams()

    return (
        <Router>
			<Switch>
				<Route path="/stuffs">
					<Stuff />
				</Route>
				<Route path="/u/:userReference">
					<Profile />
				</Route>
				<Route path="/settings">
					<Settings />
				</Route>
				<Route path="/signin">
					<Signin />
				</Route>
				<Route path="/signup">
					<Signup />
				</Route>
				<Route path="/quiz/:quizId">
					<Quiz />
				</Route>
				<Route path="/">
					<App />
				</Route>
			</Switch>
			<Modal />
		</Router>
    )
}

export default URouter
import { 
    MODAL_SHOW,
    MODAL_HIDE,
    MODAL_CLEAR,
    MODAL_SET
} from "../actions/modal"

const defaultState = {}

const modal = (state = defaultState, action) => {
    switch (action.type) {
        case MODAL_SHOW:
            return Object.assign({}, state, {
                active: true,
                component: action.component
            })

        case MODAL_HIDE:
            return Object.assign({}, state, {
                active: false
            })

        case MODAL_CLEAR:
            return defaultState

        case MODAL_SET:
            return Object.assign({}, state, action.params)

        default:
            return state
    }
}

export default modal
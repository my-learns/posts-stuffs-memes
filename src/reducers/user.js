import { 
    AUTH_SIGNIN_SUCCESS,
    AUTH_SIGNIN_PENDING,
    AUTH_SIGNIN_FAILED,
    AUTH_SIGNUP_PENDING,
    AUTH_SIGNUP_SUCCESS,
    AUTH_SIGNUP_FAILED,
    AUTH_SIGNOUT_SUCCESS,
} from "../actions/User";

const defaultState = {
    loggedIn: false,
}

const user = (state = defaultState, action) => {
    switch (action.type) {
        case AUTH_SIGNIN_PENDING:
            return Object.assign({}, state, {
                authSigninPending: true,
                authSigninFailed: false,
            })
        
        case AUTH_SIGNIN_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: true,
                authSigninPending: false,
                authSigninFailed: false,
                ...action.user
            })

        case AUTH_SIGNIN_FAILED:
            return Object.assign({}, state, {
                isAuthenticated: false,
                authSigninPending: false,
                authSigninFailed: true,
                authSigninFailedMessage: action.message
            })

        case AUTH_SIGNOUT_SUCCESS:
            console.log("signout")
            return defaultState;
            // return Object.assign({}, state, {
            //     loggedIn: false,
                
            // })

        case AUTH_SIGNUP_PENDING:
            return Object.assign({}, state, {
                authSignupPending: true,
            })

        case AUTH_SIGNUP_SUCCESS:
            return Object.assign({}, state, {
                authSignupSuccess: true,
                authSignupPending: false,
                authSignupFailed: false,
            })

        case AUTH_SIGNUP_FAILED:
            return Object.assign({}, state, {
                authSignupSuccess: false,
                authSignupPending: false,
                authSignupFailed: true,
                authSignupFailedMessage: action.message
            })

        default:
            return state;
    }
}

export default user
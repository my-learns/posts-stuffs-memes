import { 
    BUFFER_ATTACH_FILE,
    BUFFER_CLEAR
} from "../actions/filebuffer";

const defaultState = {}

const filebuffer = (state = defaultState, action) => {
    switch (action.type) {
        case BUFFER_ATTACH_FILE:
            return Object.assign({}, state, {
                file: action.file,
                fileLocalUrl: URL.createObjectURL(action.file)
            })
    
        case BUFFER_CLEAR:
            return defaultState

        default:
            return state;
    }
}

export default filebuffer
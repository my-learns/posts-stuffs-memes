import { 
    POST_LIKE, 
    POST_UNLIKE, 
    POST_DISLIKE, 
    POST_UNDISLIKE, 
} from '../actions'

import {
    RECEIVE_POSTS, POST_ADD_HASTAGS
} from '../actions/PostActions'
import { POST_LIKE_PENDING, POST_LIKE_SUCCESS } from '../actions/Post';

const defaultState = {
    posts: []
}

const Posts = (state = defaultState, action) => {
    switch(action.type) {
        case POST_LIKE:
            return state.map((post) => {
                if (post.post_ref === action.id && !post.liked) {
                    if (post.disliked) {
                        return Object.assign({}, post, {liked: true, likes: post.likes+1, disliked: false, dislikes: post.dislikes-1})
                    }
                    return Object.assign({}, post, {liked: true, likes: post.likes+1})
                }
                return post
            });
        case POST_DISLIKE:
            return state.map((post) => {
                if (post.post_ref === action.id && !post.disliked) {
                    if (post.liked) {
                        return Object.assign({}, post, {liked: false, likes: post.likes-1, disliked: true, dislikes: post.dislikes+1})
                    }
                    return Object.assign({}, post, {disliked: true, dislikes: post.dislikes+1})
                }
                return post
            });
        case POST_UNLIKE:
            return state.map((post) => {
                if (post.post_ref === action.id && post.liked) {
                    return Object.assign({}, post, {liked: false, likes: post.likes-1})
                }
                return post
            });
        case POST_UNDISLIKE:
            return state.map((post) => {
                if (post.post_ref === action.id && post.disliked) {
                    return Object.assign({}, post, {disliked: false, dislikes: post.dislikes-1})
                }
                return post
            });

        case RECEIVE_POSTS:
            return Object.assign({}, state, {posts: action.posts});

        case POST_LIKE_PENDING:
            return Object.assign({}, state, {
                likePending: true
            })

        case POST_LIKE_SUCCESS:
            return Object.assign({}, state, {
                posts: state.posts.map((post) => {
                    if (post.id !== action.postId) return post;
                    return Object.assign({}, post, {
                        likes: action.postLikes,
                        liked: true
                    })
                })
            })

        case POST_ADD_HASTAGS:
            return state.map((post) => {
                if (post.postId === action.postId) {
                    return Object.assign({}, post, {hashtags: action.hashtags})
                }
                return post;
            })
        default: 
            return state;
    }
}

export default Posts;
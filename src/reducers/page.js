import { PAGE_SCROLL_ENABLE, PAGE_SCROLL_DISABLE } from "../actions/Actions";

const defaultState = {};

const PageReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PAGE_SCROLL_ENABLE:
            return Object.assign({}, state, {scrollDisabled: false});

        case PAGE_SCROLL_DISABLE:
            return Object.assign({}, state, {scrollDisabled: true})
    
        default:
            return state
    }
}

export default PageReducer
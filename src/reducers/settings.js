import { 
    SETTS_FIELD_UPDATE_PENDING,
    SETTS_FIELD_UPDATE_SUCCESS,
    SETTS_FIELD_UPDATE_FAILED,
    SETTS_FIELD_GET,
    SETTS_FIELD_SET_VALUE
} from "../actions/Settings";

const defaultState = {
    fields: []
}

const settings = (state = defaultState, action) => {
    switch (action.type) {
        case SETTS_FIELD_UPDATE_PENDING:
            return Object.assign({}, state, {
                fields: state.fields.map((field) => {
                    if (field.name === action.fieldName) {
                        return Object.assign({}, field, {
                            updatePending: true,
                            updateSuccess: false,
                            updateFailed: false
                        })
                    }
                    return field
                })
            });
        
        case SETTS_FIELD_UPDATE_SUCCESS:
            return Object.assign({}, state, {
                fields: state.fields.map((field) => {
                    if (field.name === action.fieldName) {
                        return Object.assign({}, field, {
                            changed: false,
                            updateSuccess: true,
                            updatePending: false,
                            updateFailed: false
                        })
                    }
                    return field
                })
            });
    
        case SETTS_FIELD_UPDATE_FAILED:
            return Object.assign({}, state, {
                fields: state.fields.map((field) => {
                    if (field.name === action.fieldName) {
                        return Object.assign({}, field, {
                            updateSuccess: false,
                            updatePending: false,
                            updateFailed: true,
                            updateFailedMessage: action.message
                        })
                    }
                    return field
                })
            });

        case SETTS_FIELD_GET:
            return Object.assign({}, state, {
                fields: [
                    ...state.fields,
                    {
                        name: action.fieldName,
                    value: action.value
                    }
                ]
            })

        case SETTS_FIELD_SET_VALUE:
            return Object.assign({}, state, {
                fields: state.fields.map((field) => {
                    if (field.name === action.fieldName) {
                        console.log(field);
                        return Object.assign({}, field, {
                            value: action.value,
                            changed: true
                        })
                    }
                    return field
                }),
            })

        default:
            return state
    }
}

export default settings
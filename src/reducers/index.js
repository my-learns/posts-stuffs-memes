import { combineReducers } from 'redux'
import posts from './posts'
import addpost from './addpost'
import page from './page'
import user from './user'
import profile from './profile'
import settings from './settings.js'
import filebuffer from './filebuffer.js'
import modal from './modal'
import quiz from './quiz'

export default combineReducers({
    posts,
    addpost,
    page,
    user,
    profile,
    settings,
    filebuffer,
    modal,
    quiz
});
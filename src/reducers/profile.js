import { USER_PROFILE_PENDING, USER_PROFILE_SUCCESS, USER_PROFILE_FAILED } from "../actions/User";

const defaultState = {}

const profile = (state = defaultState, action) => {
    switch (action.type) {
        case USER_PROFILE_PENDING:
            return Object.assign({}, state, {
                userProfilePending: true
            })

        case USER_PROFILE_SUCCESS:
            console.log(action)
            return Object.assign({}, state, {
                userProfileSuccess: true,
                userProfilePending: false,
                userProfileFailed: false,
                ...action.user
            })
            
        case USER_PROFILE_FAILED:
            return Object.assign({}, state, {
                userProfilePending: false,
                userProfileSuccess: false,
                userProfileFailed: true,
                userProfileFailedMessage: action.message
            })
            
        default:
            return state;
    }
}

export default profile
import { QUIZ_GET_PENDING, QUIZ_GET_SUCCESS } from '../actions/Quiz'

const defaultState = {}

const Quiz = (state = defaultState, action) => {
    switch(action.type) {
        case QUIZ_GET_PENDING: 
            return Object.assign({}, state, {
                pending: true
            })

        case QUIZ_GET_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                success: true,
                failed: false,
                ...action.quiz
            })

        default:
            return state
    }
}

export default Quiz
import { 
    POSTADD_HIDE,
    POSTADD_SHOW,
    POSTADD_UPDATE_DESCRIPTION,
    POSTADD_REMOVE_HASHTAG,
    POSTADD_CHANGE_HASHTAG,
    POSTADD_ADD_HASHTAG,
} from "../actions";
import { 
    POST_ADD_PENDING,
    POST_ADD_SUCCESS,
    POST_ADD_FAILED,
    POST_ADD_FILE_ASSIGN, 
    POST_ADD_FILE_UPLOAD_PENDING,
    POST_ADD_FILE_UPLOAD_SUCCESS,
    POST_ADD_FILE_UPLOAD_FAILED
} from "../actions/Post";

const defaultState = {
    active: false,
    postAddDescription: "",
    postAddHashtags: [],
    data: {
        description: "",
        hashtags: [],
        image: false
    },
    currentHastag: "",
    imageUrl: ""
}

const addpost = (state = defaultState, action) => {
    switch(action.type) {
        case POSTADD_SHOW:
            return Object.assign({}, state, {active: true})
        case POSTADD_HIDE:
            return Object.assign({}, state, {active: false})

        case POSTADD_UPDATE_DESCRIPTION:
            return Object.assign({}, state, {
                postAddDescription: action.text
            })

        case POSTADD_REMOVE_HASHTAG:
            return Object.assign({}, state, {
                postAddHashtags: state.data.hashtags.filter((hashtag) => (hashtag !== action.text))
            })

        case POSTADD_CHANGE_HASHTAG:
            return Object.assign({}, state, {
                currentHashtag: action.text
            })

        case POSTADD_ADD_HASHTAG:
            let hashtags = state.postAddHashtags;
            hashtags.push(action.text)
            return Object.assign({}, state, {
                hashtags: hashtags,
                currentHashtag: ""
            })

        case POST_ADD_PENDING: 
            return Object.assign({}, state, {
                postAddPending: true,
                postAddFailded: false,
                postAddSuccess: false,
            })
        
        case POST_ADD_SUCCESS:
            return Object.assign({}, state, {
                postAddSuccess: true,
                postAddPending: false,
                postAddFailded: false,
                active: false,
                postAddHashtags: [],
                postAddDescription: "",
                currentHashtag: "",
                postAddFileAssigned: false,
            })

        case POST_ADD_FAILED:
            return Object.assign({}, state, {
                postAddFailed: true,
                postAddFailedMessage: action.message,
                postAddSuccess: false,
                postAddPending: false
            })

        case POST_ADD_FILE_UPLOAD_PENDING:
            return Object.assign({}, state, {
                postAddSubmited: true,
                postAddFileUploadPending: true,
                postAddFileUploadSuccess: false,
            })

        case POST_ADD_FILE_UPLOAD_SUCCESS:
            return Object.assign({}, state, {
                postAddFileUploadSuccess: true,
                postAddFileUploadPending: false,
                postAddFileUploadFailed: false,
                postAddFileUploadFilename: action.filename
            })

        case POST_ADD_FILE_UPLOAD_FAILED:
            return Object.assign({}, state, {
                postAddFileUploadFailed: true,
                postAddFileUploadFailedMessage: action.message,
                postAddFileUploadPending: false,
                postAddFileUploadSuccess: false
            })

        case POST_ADD_FILE_ASSIGN:
            return Object.assign({}, state, {
                postAddFileAssigned: true,
                postAddFileLocalUrl: URL.createObjectURL(action.file),
                postAddFile: action.file
            })

        default: 
            return state
    }
}

export default addpost;
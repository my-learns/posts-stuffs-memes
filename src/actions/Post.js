import Axios from "axios"

export const POST_ADD_PENDING = "POST_ADD_PENDING"
export const postAddPending = () => ({
    type: POST_ADD_PENDING
})

export const POST_ADD_SUCCESS = "POST_ADD_SUCCESS"
export const postAddSuccess = (data) => ({
    type: POST_ADD_SUCCESS
})

export const POST_ADD_FAILED = "POST_ADD_FAILED"
export const postAddFailed = (message) => ({
    type: POST_ADD_FAILED
})

export const postAddUpload = (data) => (
    dispatch => {
        dispatch(postAddPending())
        Axios
            .post('/post/add', data)
            .then((response) => {
                if(response.data.ok) {
                    dispatch(postAddSuccess(response.data))
                } else {
                    dispatch(postAddFailed(response.data.message))
                }
            })
            .catch((err) => console.log(err))
    }
)



export const POST_ADD_FILE_ASSIGN = "POST_ADD_FILE_ASSIGN"
export const postAddFileAssign = (file) => ({
    type: POST_ADD_FILE_ASSIGN,
    file: file
})

export const POST_ADD_FILE_UPLOAD_PENDING = "POST_ADD_FILE_UPLOAD_PENDING"
export const postAddFileUploadPending = () => ({
    type: POST_ADD_FILE_UPLOAD_PENDING
})

export const POST_ADD_FILE_UPLOAD_SUCCESS = "POST_ADD_FILE_UPLOAD_SUCCESS"
export const postAddFileUploadSuccess = (filename) => ({
    type: POST_ADD_FILE_UPLOAD_SUCCESS,
    filename: filename
})

export const POST_ADD_FILE_UPLOAD_FAILED = "POST_ADD_FILE_UPLOAD_FAILDE"
export const postAddFileUploadFailed = (message) => ({
    type: POST_ADD_FILE_UPLOAD_FAILED
})

export const postAddFileUpload = (file) => (
    dispatch => {
        dispatch(postAddFileUploadPending());
        const formData = new FormData();
        formData.append('file', file);
        Axios
            .post('/upload', formData)
            .then((response) => {
                if (response.data.ok) {
                    dispatch(postAddFileUploadSuccess(response.data.filename))
                } else {
                    dispatch(postAddFileUploadFailed(response.data.message))
                }
            })
    }
)




export const POST_LIKE_PENDING = "POST_LIKE_PENDING"
export const postLikePending = (postId) => ({
    type: POST_LIKE_PENDING,
    postId: postId,
})

export const POST_LIKE_SUCCESS = "POST_LIKE_SUCCESS"
export const postLikeSuccess = (postId, postLikes) => ({
    type: POST_LIKE_SUCCESS,
    postId: postId,
    postLikes: postLikes
})

export const POST_LIKE_FAILED = "POST_LIKE_FAILED"
export const postLikeFailed = (postId, message) => ({
    type: POST_LIKE_FAILED,
    postId: postId,
    message: message
})

export const postLike = (postId) => (
    dispatch => {
        dispatch(postLikePending(postId));
        console.log("pending");
        Axios.post(`/post/${postId}/like`, {})
            .then((response) => {
                if (response.data.ok) {
                    console.log("success");
                    dispatch(postLikeSuccess(postId, response.data.likes))
                } else {
                    console.log("failed");
                    console.log(response.data);
                    dispatch(postLikeFailed(postId, response.data.message))
                }
            })
    }
)
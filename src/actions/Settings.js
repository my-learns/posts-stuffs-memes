import Axios from "axios"

export const SETTS_FIELD_UPDATE_PENDING = "SETTS_FIELD_UPDATE_PEDING"
export const settsFieldUpdatePending = (fieldName) => ({
    type: SETTS_FIELD_UPDATE_PENDING,
    fieldName: fieldName

})

export const SETTS_FIELD_UPDATE_SUCCESS = "SETTS_FIELD_UPDATE_SUCCESS"
export const settsFieldUpdateSuccess = (fieldName) => ({
    type: SETTS_FIELD_UPDATE_SUCCESS,
    fieldName: fieldName
})

export const SETTS_FIELD_UPDATE_FAILED = "SETTS_FIELD_UPDATE_FAILED"
export const settsFieldUpdateFailed = (fieldName, message) => ({
    type: SETTS_FIELD_UPDATE_FAILED,
    fieldName: fieldName,
    message: message
})

export const settsFieldUpdate = (fieldName, value) => (
    dispatch => {
        dispatch(settsFieldUpdatePending())
        Axios.post(`/setts/${fieldName}`, {value: value})
            .then((response) => {
                if (response.data.ok) {
                    dispatch(settsFieldUpdateSuccess(fieldName))
                } else {
                    dispatch(settsFieldUpdateFailed(fieldName, 'err'))
                }
            })
    }
)


export const SETTS_FIELD_GET = "SETTS_FIELD_GET"
export const settsFieldGet = (fieldName, value) => ({
    type: SETTS_FIELD_GET,
    fieldName: fieldName,
    value: value
})

export const SETTS_FIELD_SET_VALUE = "SETTS_FIELD_SET_VALUE"
export const settsFieldSetValue = (fieldName, value) => ({
    type: SETTS_FIELD_SET_VALUE,
    fieldName: fieldName,
    value: value
})

export const settsField = (fieldName) => (
    dispatch => Axios.get(`/setts/${fieldName}`).then((response) => dispatch(settsFieldGet(fieldName, response.data.value)))
)


export const SETTS_PROFILE_IMAGE_UPLOAD_PENDING = "SETTS_PROFILE_IMAGE_PENDING"
export const settsProfileImageUploadPending = () => ({
    type: SETTS_PROFILE_IMAGE_UPLOAD_PENDING
})

export const SETTS_PROFILE_IMAGE_UPLOAD_SUCCESS = "SETTS_PROFILE_IMAGE_UPLOAD_SUCCESS"
export const settsProfileImageUploadSuccess = () => ({
    type: SETTS_PROFILE_IMAGE_UPLOAD_SUCCESS
})

export const SETTS_PROFILE_IMAGE_UPLOAD_FAILED = "SETTS_PROFILE_IMAGE_UPLOAD_FAILED"
export const settsProfileImageUploadFailed = (message) => ({
    type: SETTS_PROFILE_IMAGE_UPLOAD_FAILED,
    message: message
})

export const settsProfileImageUpload = (image) => (
    dispatch => {
        dispatch(settsProfileImageUploadPending())
        var data = new FormData();
        data.append('file', image);
        Axios
            .post('/setts/profileimage', data)
            .then((response) => {
                console.log("resp");
                console.log(response)
                if (response.data.ok) {
                    dispatch(settsProfileImageUploadSuccess())
                } else {
                    dispatch(settsProfileImageUploadFailed())
                }
            })
    }
)
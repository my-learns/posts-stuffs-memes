export const BUFFER_ATTACH_FILE = "BUFFER_ATTACH_FILE"
export const bufferAttachFile = (file) => ({
    type: BUFFER_ATTACH_FILE,
    file: file
})

export const BUFFER_CLEAR = "BUFFER_CLEAR"
export const bufferClear = () => ({
    type: BUFFER_CLEAR
})
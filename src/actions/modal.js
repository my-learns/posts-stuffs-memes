export const MODAL_SHOW = "MODAL_SHOW"
export const modalShow = (component) => ({
    type: MODAL_SHOW,
    component: component
})

export const MODAL_HIDE = "MODAL_HIDE"
export const modalHide= () => ({
    type: MODAL_HIDE
})

export const MODAL_CLEAR = "MODAL_CLEAR"
export const modalClear = () => ({
    type: MODAL_CLEAR
})

export const MODAL_SET = "MODAL_SET"
export const modalSet = (params) => ({
    type: MODAL_SET,
    params: params
})
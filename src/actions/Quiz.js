import Axios from "axios"

export const QUIZ_GET = "QUIZ_GET"
export const quizGet = (quizId) => (
    dispatch => {
        dispatch(quizGetPending());
        Axios
            .get(`/quiz/${quizId}`)
            .then((resp) => {
                dispatch(quizGetSuccess(resp.data))
            })
    }
)

export const QUIZ_GET_PENDING = "QUIZ_GET_PENDING"
export const quizGetPending = () => ({
    type: QUIZ_GET_PENDING
})

export const QUIZ_GET_SUCCESS = "QUIZ_GET_SUCCESS"
export const quizGetSuccess = (quiz) => ({
    type: QUIZ_GET_SUCCESS,
    quiz: quiz
})
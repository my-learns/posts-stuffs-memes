import axios from 'axios'

export const fetchPosts = () => {
    return dispatch => (
        axios
            .get("/post/all")
            .then((response) => {
                if (response.data.ok) {
                    dispatch(receivePosts(response.data.posts))
                } else {
                    console.log("Error")
                }
            })
            .catch((err) => console.log(err))
    )
}

export const RECEIVE_POSTS = "RECEIVE_POSTS"
export const receivePosts = (posts) => ({
    type: RECEIVE_POSTS,
    posts: posts
})


export const postGetHashtags = (postId) => {
    return dispatch => (
        axios
            .get(`/post/${postId}/hashtags`)
            .then((res) => dispatch(postAddHashtags(postId, res.data)))
            .catch((err) => console.log("Eroare"))
    )
    }

export const POST_ADD_HASTAGS = "POST_ADD_HASHTAGS"
export const postAddHashtags = (postId, hashtags) => ({
    type: POST_ADD_HASTAGS,
    postId: postId,
    hashtags: hashtags,
})
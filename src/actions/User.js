import Axios from "axios"

export const AUTH_SIGNIN_PENDING = "USER_LOGIN_PENDING"
export const authSigninPending = () => ({
    type: AUTH_SIGNIN_PENDING
})

export const AUTH_SIGNIN_SUCCESS = "USER_LOGIN_SUCCESS"
export const authSigninSuccess = (data) => ({
    type: AUTH_SIGNIN_SUCCESS,
    user: data
})

export const AUTH_SIGNIN_FAILED = "USER_LOGIN_FAILED"
export const authSigninFailed = (message) => ({
    type: AUTH_SIGNIN_FAILED,
    message: message
})

export const authSignin = (userData) => (
    dispatch => {
        dispatch(authSigninPending());
        Axios
            .post('/auth/signin', userData, {})
            .then((response) => {
                if (response.data.ok) {
                    localStorage.setItem('token', response.data.token);
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token
                    dispatch(authSigninSuccess(response.data))
                } else {
                    dispatch(authSigninFailed(response.data.message))
                }
            })
            .catch((err) => console.log(err))
    }
)

export const AUTH_SIGNUP_PENDING = "AUTH_SIGNUP_PENDING"
export const authSignupPending = () => ({
    type: AUTH_SIGNUP_PENDING
})

export const AUTH_SIGNUP_SUCCESS = "AUTH_SIGNUP_SUCCESS"
export const authSignupSuccess = () => ({
    type: AUTH_SIGNUP_SUCCESS
})

export const AUTH_SIGNUP_FAILED = "AUTH_SIGNUP_FAILED"
export const authSignupFailed = (message) => ({
    type: AUTH_SIGNUP_FAILED,
    message: message
})

export const authSignup = (userData) => (
    dispatch => {
        dispatch(authSignupPending())
        Axios
            .post('/auth/signup', userData)
            .then((response) => {
                if (response.data.ok) {
                    localStorage.setItem('token', response.data.token)
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token
                    dispatch(authSignupSuccess())
                } else {
                    dispatch(authSignupFailed(response.data.message))
                }
            })
    }
)


export const AUTH_SIGNOUT_SUCCESS = "AUTH_SIGNOUT_SUCCESS"
export const authSignoutSuccess = () => ({
    type: AUTH_SIGNOUT_SUCCESS
})

export const authSignout = () => (
    dispatch => (
        Axios
            .get("/auth/signout")
            .then((response) => {
                localStorage.removeItem('token')
                Axios.defaults.headers.common['Authorization'] = '';
                dispatch(authSignoutSuccess())
            })
    )
)

export const authPing = () => (
    dispatch => (
        Axios
            .get('/auth/ping')
            .then((response) => {
                if (response.data.ok) {
                    dispatch(authSigninSuccess({}))
                }
            })
    )
)




export const meRequestInfo = () => (
    dispatch => (
        Axios
            .get("/u.me")
            .then((response) => {
                if (response.data.ok) {
                    dispatch(authSigninSuccess(response.data.user))
                }
            })
    )
)


export const USER_PROFILE_PENDING = "USER_PROFILE_PENDINDG"
export const userProfilePending = () => ({
    type: USER_PROFILE_PENDING
})

export const USER_PROFILE_SUCCESS = "USER_PROFILE_SUCCESS" 
export const userProfileSuccess = (userData) => ({
    type: USER_PROFILE_SUCCESS,
    user: userData,    
})

export const USER_PROFILE_FAILED = "USER_PROFILE_FAILED"
export const userProfileFailed = (message) => ({
    type: USER_PROFILE_FAILED,
    message: message
})

export const userProfile = (userRef) => (
    dispatch => {
        dispatch(userProfilePending())
        return (
            Axios
                .get(`/user/${userRef}`)
                .then((response) => {
                    if (response.data.ok) {
                        dispatch(userProfileSuccess(response.data.user))
                    } else {
                        dispatch(userProfileSuccess(response.data.message))
                    }
                })
        )
    }
)
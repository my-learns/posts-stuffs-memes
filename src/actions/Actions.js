import axios from 'axios'

export const POST_LIKE = "POST_LIKE"
export const POST_UNLIKE = "POST_UNLIKE"
export const POST_DISLIKE = "POST_DISLIKE"
export const POST_UNDISLIKE = "POST_UNDISLIKE"
// const POST_LIKE = "POST_LIKE"

export const postLike = (id) => ({
    type: POST_LIKE,
    id: id
})

export const postUnlike = (id) => ({
    type: POST_UNLIKE,
    id: id
})

export const postDislike = (id) => ({
    type: POST_DISLIKE,
    id: id
})

export const postUndislike = (id) => ({
    type: POST_UNDISLIKE,
    id: id
})

export const POSTADD_CREATE = "POSTADD_CREATE"
export const POSTADD_SHOW = "POSTADD_SHOW"
export const POSTADD_HIDE = "POSTADD_HIDE"
export const POSTADD_UPDATE_DESCRIPTION = "POSTADD_UPDATE_DESCRIPTION"
export const POSTADD_REMOVE_HASHTAG = "POSTADD_REMOVE_HASHTAG"
export const POSTADD_ADD_HASHTAG = "POSTADD_ADD_HASHTAG"
export const POSTADD_CHANGE_HASHTAG = "POSTADD_CHANGE_HASHTAG"

export const postaddCreate = (post) => ({
    type: POSTADD_CREATE,
    post
})

export const postaddShow = () => ({
    type: POSTADD_SHOW
})

export const postaddHide = () => ({
    type: POSTADD_HIDE
})

export const postaddUpdateDescription = (text) => ({
    type: POSTADD_UPDATE_DESCRIPTION,
    text: text
})

export const postaddRemoveHashtag = (text) => ({
    type: POSTADD_REMOVE_HASHTAG,
    text: text
})

export const postaddAddHashtag = (text) => ({
    type: POSTADD_ADD_HASHTAG,
    text: text
})

export const postaddChangeHashtag = (text) => ({
    type: POSTADD_CHANGE_HASHTAG,
    text: text
})

export const POSTADD_SUBMIT = "POSTADD_SUBMIT"
export const postaddSubmit = (data) => {
    const formData = new FormData();
    formData.append("file", data.image);
    axios
        .post("/upload", formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then((resp) => {
            data.image = resp.data.filename;
            console.log(data);

            axios
                .post("/post", data, {})
                .then((resp) => {console.log(resp)})
                .catch((err) => {console.log(err)})
        })
        .catch(err => {console.log(`Error -> ${err}`)})
}

export const POSTADD_IMAGE_UPLOAD = "POSTADD_UPLOAD_IMAGE"
export const postaddImageUpload = (image, imageUrl) => ({
    type: POSTADD_IMAGE_UPLOAD,
    image: image,
    imageUrl: imageUrl,
})



export const PAGE_SCROLL_ENABLE = "PAGE_SCROLL_ENABLE"
export const pageScrollEnable = () => ({
    type: PAGE_SCROLL_ENABLE
})

export const PAGE_SCROLL_DISABLE = "PAGE_SCROLL_DISABLE"
export const pageScrollDisable = () => ({
    type: PAGE_SCROLL_DISABLE
})

export const POSTS_GET_ALL = "POSTS_GET_ALL"
export const postsGetAll = () => {
    // axios
        // .get("/post")
        // .then(())
    // type: POSTS_GET_ALL
}


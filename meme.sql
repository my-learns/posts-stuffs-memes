/*
Navicat MySQL Data Transfer

Source Server         : localhost-ospanel
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : meme

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-04-29 23:00:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for favicons
-- ----------------------------
DROP TABLE IF EXISTS `favicons`;
CREATE TABLE `favicons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of favicons
-- ----------------------------
INSERT INTO `favicons` VALUES ('7', 'https://google.com', '1591549562685.ico', '2020-06-07 17:06:03', '2020-06-07 17:06:03');
INSERT INTO `favicons` VALUES ('8', 'https://stackoverflow.com', '1591549691143.ico', '2020-06-07 17:08:11', '2020-06-07 17:08:11');
INSERT INTO `favicons` VALUES ('9', 'https://allmy.000webhostapp.com', '1591549771862.ico', '2020-06-07 17:09:32', '2020-06-07 17:09:32');
INSERT INTO `favicons` VALUES ('10', 'http://allmy.000webhostapp.com', '1591549787537.ico', '2020-06-07 17:09:47', '2020-06-07 17:09:47');
INSERT INTO `favicons` VALUES ('11', 'https://odnoklassniki.ru', '1591552620535.ico', '2020-06-07 17:57:01', '2020-06-07 17:57:01');
INSERT INTO `favicons` VALUES ('12', 'https://css-tricks.com', '1591553110349.png', '2020-06-07 18:05:10', '2020-06-07 18:05:10');
INSERT INTO `favicons` VALUES ('13', 'https://facebook.com', '1591553159996.ico', '2020-06-07 18:06:00', '2020-06-07 18:06:00');
INSERT INTO `favicons` VALUES ('14', 'http://facebook.com', '1591553205052.ico', '2020-06-07 18:06:45', '2020-06-07 18:06:45');
INSERT INTO `favicons` VALUES ('15', 'https://ok.ru', '1598093057841.ico', '2020-08-22 10:44:18', '2020-08-22 10:44:18');
INSERT INTO `favicons` VALUES ('16', 'https://ok.ru', '1598093058496.ico', '2020-08-22 10:44:18', '2020-08-22 10:44:18');
INSERT INTO `favicons` VALUES ('17', 'https://instagram.com', '1598093096095.ico', '2020-08-22 10:44:57', '2020-08-22 10:44:57');
INSERT INTO `favicons` VALUES ('18', 'https://unifun.com', '1619726078234.png', '2021-04-29 19:54:38', '2021-04-29 19:54:38');
INSERT INTO `favicons` VALUES ('19', 'https://skladovka.md', '1619726102918.ico', '2021-04-29 19:55:03', '2021-04-29 19:55:03');
INSERT INTO `favicons` VALUES ('20', 'https://marche.gratis', '1619726128364.png', '2021-04-29 19:55:29', '2021-04-29 19:55:29');
INSERT INTO `favicons` VALUES ('21', 'https://b2bhint.com', '1619726176107.ico', '2021-04-29 19:56:16', '2021-04-29 19:56:16');
INSERT INTO `favicons` VALUES ('22', 'https://twitter.com', '1619726238162.ico', '2021-04-29 19:57:18', '2021-04-29 19:57:18');

-- ----------------------------
-- Table structure for hashtags
-- ----------------------------
DROP TABLE IF EXISTS `hashtags`;
CREATE TABLE `hashtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hashtags
-- ----------------------------
INSERT INTO `hashtags` VALUES ('3', 'epta', '2020-05-15 18:54:00', '2020-05-15 18:54:00');
INSERT INTO `hashtags` VALUES ('4', 'eblan', '2020-05-15 18:54:00', '2020-05-15 18:54:00');
INSERT INTO `hashtags` VALUES ('5', 'advetising', '2020-05-15 19:27:04', '2020-05-15 19:27:04');
INSERT INTO `hashtags` VALUES ('6', 'nature', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `hashtags` VALUES ('7', 'flowers', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `hashtags` VALUES ('8', 'nice', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `hashtags` VALUES ('9', 'cool', '2020-05-16 09:51:59', '2020-05-16 09:51:59');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('3', '1589568840549-Screenshot_1.png', '2020-05-15 18:54:00', '2020-05-15 18:54:00');
INSERT INTO `images` VALUES ('6', '1589570823956-Untitled-1.png', '2020-05-15 19:27:04', '2020-05-15 19:27:04');
INSERT INTO `images` VALUES ('7', '1589576855949-94779304_150811049770326_6253510930307481600_o.jpg', '2020-05-15 21:07:37', '2020-05-15 21:07:37');
INSERT INTO `images` VALUES ('8', '1589622718860-Uqal21.jpg', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `images` VALUES ('9', '1589788641409-C590D922-C343-468C-91DE-434D02F097C0.jpeg', '2020-05-18 07:57:24', '2020-05-18 07:57:24');

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `postId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of likes
-- ----------------------------
INSERT INTO `likes` VALUES ('8', '8', '13', '2020-05-20 14:12:13', '2020-05-20 14:12:13');
INSERT INTO `likes` VALUES ('9', '8', '12', '2020-05-20 14:13:41', '2020-05-20 14:13:41');
INSERT INTO `likes` VALUES ('10', '8', '11', '2020-05-20 14:14:54', '2020-05-20 14:14:54');
INSERT INTO `likes` VALUES ('11', '8', '4', '2020-05-20 16:21:20', '2020-05-20 16:21:20');
INSERT INTO `likes` VALUES ('12', '8', '7', '2020-05-20 16:21:24', '2020-05-20 16:21:24');
INSERT INTO `likes` VALUES ('13', '1', '13', '2020-05-20 16:25:28', '2020-05-20 16:25:28');
INSERT INTO `likes` VALUES ('14', '8', '14', '2020-05-20 16:28:50', '2020-05-20 16:28:50');
INSERT INTO `likes` VALUES ('15', '1', '7', '2020-05-20 21:10:55', '2020-05-20 21:10:55');
INSERT INTO `likes` VALUES ('16', '1', '14', '2020-05-21 07:10:26', '2020-05-21 07:10:26');
INSERT INTO `likes` VALUES ('17', '1', '9', '2020-05-21 08:03:49', '2020-05-21 08:03:49');
INSERT INTO `likes` VALUES ('18', '1', '12', '2020-05-27 10:00:57', '2020-05-27 10:00:57');
INSERT INTO `likes` VALUES ('19', '7', '14', '2020-08-22 10:42:11', '2020-08-22 10:42:11');
INSERT INTO `likes` VALUES ('20', '7', '15', '2020-08-22 10:47:32', '2020-08-22 10:47:32');

-- ----------------------------
-- Table structure for posthashtags
-- ----------------------------
DROP TABLE IF EXISTS `posthashtags`;
CREATE TABLE `posthashtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postId` int(11) DEFAULT NULL,
  `hashtagId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posthashtags
-- ----------------------------
INSERT INTO `posthashtags` VALUES ('1', '4', '3', '2020-05-15 18:54:00', '2020-05-15 18:54:00');
INSERT INTO `posthashtags` VALUES ('2', '4', '4', '2020-05-15 18:54:00', '2020-05-15 18:54:00');
INSERT INTO `posthashtags` VALUES ('3', '7', '5', '2020-05-15 19:27:04', '2020-05-15 19:27:04');
INSERT INTO `posthashtags` VALUES ('4', '9', '6', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `posthashtags` VALUES ('5', '9', '7', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `posthashtags` VALUES ('6', '9', '8', '2020-05-16 09:51:59', '2020-05-16 09:51:59');
INSERT INTO `posthashtags` VALUES ('7', '9', '9', '2020-05-16 09:51:59', '2020-05-16 09:51:59');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT '',
  `filename` varchar(255) DEFAULT '',
  `likes` int(11) DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('4', '1', 'text', '1589568840549-Screenshot_1.png', '126', '2020-05-15 18:54:00', '2020-05-20 16:21:21');
INSERT INTO `posts` VALUES ('7', '7', 'second post', '1589570823956-Untitled-1.png', '2', '2020-05-15 19:27:04', '2020-05-20 21:10:55');
INSERT INTO `posts` VALUES ('8', '7', 'Порву как тузик грелку', '1589576855949-94779304_150811049770326_6253510930307481600_o.jpg', '0', '2020-05-15 21:07:38', '2020-05-15 21:07:38');
INSERT INTO `posts` VALUES ('9', '1', 'O postare cât o mie de cuvinte!', '1589622718860-Uqal21.jpg', '1', '2020-05-16 09:51:59', '2020-05-21 08:03:49');
INSERT INTO `posts` VALUES ('10', '8', 'text', '1589901359056-127a1b14dcca7b0ea0aa7fdd156d1a1a.jpg', '0', '2020-05-19 15:16:00', '2020-05-19 15:16:00');
INSERT INTO `posts` VALUES ('11', '8', 'Postare noua de jorika', '1589911948663-vector-forest-sunset-forest-sunset-forest-wallpaper-thumb.jpg', '2', '2020-05-19 18:12:28', '2020-05-20 14:14:54');
INSERT INTO `posts` VALUES ('12', '7', 'Post by Sanioook', '1589912396544-wp3198779.jpg', '3', '2020-05-19 18:19:56', '2020-05-27 10:00:58');
INSERT INTO `posts` VALUES ('13', '8', 'Awesome nature!!!', '1589925614431-D1A7A0ED-0CD5-4227-86E5-904551A55802.jpeg', '3', '2020-05-19 22:00:21', '2020-05-20 16:25:28');
INSERT INTO `posts` VALUES ('14', '1', 'Agagaga', '1589992073234-5C328932-2BCA-4207-9158-881E4434F2C6.jpeg', '3', '2020-05-20 16:27:55', '2020-08-22 10:42:11');
INSERT INTO `posts` VALUES ('16', '1', 'ehehe', '1619726025797-pexels-photo-268533.jpeg', '0', '2021-04-29 19:53:49', '2021-04-29 19:53:49');

-- ----------------------------
-- Table structure for resources
-- ----------------------------
DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resources
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `privileges` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------

-- ----------------------------
-- Table structure for sequelizemeta
-- ----------------------------
DROP TABLE IF EXISTS `sequelizemeta`;
CREATE TABLE `sequelizemeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sequelizemeta
-- ----------------------------
INSERT INTO `sequelizemeta` VALUES ('20200513163654-create-post.js');
INSERT INTO `sequelizemeta` VALUES ('20200515180654-create-hashtag.js');
INSERT INTO `sequelizemeta` VALUES ('20200515183323-create-image.js');
INSERT INTO `sequelizemeta` VALUES ('20200515184036-create-post-hashtag.js');
INSERT INTO `sequelizemeta` VALUES ('20200516104111-create-user.js');
INSERT INTO `sequelizemeta` VALUES ('20200516104156-create-role.js');
INSERT INTO `sequelizemeta` VALUES ('20200516104344-create-resource.js');
INSERT INTO `sequelizemeta` VALUES ('20200516104603-create-like.js');
INSERT INTO `sequelizemeta` VALUES ('20200607163229-create-favicon.js');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `profileImageFilename` varchar(255) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'saniok', 'saniok.i.am@gmail.com', 'Sandu', 'Morari', 'saniok2000', '+37376700062', '1590352476690-95C332D9-9CA4-4B54-946B-57D37DAF456A.jpeg', '1', '2020-05-16 13:54:34', '2020-05-28 17:51:46');
INSERT INTO `users` VALUES ('7', 'eptanick', 'sandu.m.md@gmail.com', 'Опасный', 'Поцык', 'saniok2000', null, 'patan.jpg', '2', '2020-05-17 16:39:08', '2020-08-22 10:49:20');
INSERT INTO `users` VALUES ('8', 'jorikel', 'jorik@cardan.md', 'Jora', 'Cardan', 'saniok2000', null, '1590774137313-Uqal21.jpg', '2', '2020-05-17 17:37:12', '2020-06-15 15:36:00');
